<?php

class database extends mysqli{
	/*private $DB_HOST = 'localhost';
	private $DB_USER = 'root';
	private $DB_PASS = '';
	private $DB_NAME = 'gt2';*/
	private $DB_HOST = '107.180.51.37';
	private $DB_NAME = 'genesis_tech';
	private $DB_USER = 'adminGenesisTech';
	private $DB_PASS = 'VU.Jo^wv}#MK';

	public function __construct(){
		parent::__construct($this->DB_HOST,$this->DB_USER,$this->DB_PASS,$this->DB_NAME);
		if(mysqli_connect_errno()){
			printf("Fallo la conexión: %s", mysqli_connect_error());
			exit();
		}

		if(!$this->set_charset("utf8")){
			printf("Fallo utf-8 %s", $this->error);
			exit();
		}
	}

	public function select($tabla, $datos){
		$sql = "SELECT 
					* 
				FROM 
					$tabla 
				WHERE 1";
		foreach ($datos as $key => $value) {
			//Si es un arreglo, se usa el IN
			if(is_array($value)){
				$sql .= " AND $key IN ('".implode("', '", $value)."')";
			} else {
				$sql .= " AND $key = '$value'";
			}
		}
		return $this->ejecutarConsulta($sql);
	}

	public function insert($tabla, $datos){
		$sql = "INSERT INTO 
					$tabla 
				SET ";
		foreach ($datos as $key => $value) {
			$sql .= "$key = '$value',";
		}
		$sql .= "creado_por = ".$_SESSION['usuario']['id'].", fecha_creacion=NOW()";
		return $this->ejecutarConsulta($sql);
	}

	public function update($tabla, $datos){
		$sql = "UPDATE 
					$tabla 
				SET ";
		foreach ($datos as $key => $value) {
			$sql .= "$key = '$value',";
		}
		$sql .= "modificado_por = ".$_SESSION['usuario']['id'].", fecha_modificacion=NOW()";
		$sql .= " WHERE id = '$datos[id]'";
		return $this->ejecutarConsulta($sql);
	}

	public function cantidad($tabla, $datos){
		$sql = "SELECT 
					COUNT(1) AS cantidad
				FROM 
					$tabla
				WHERE 1";
		foreach ($datos as $key => $value) {
			$sql .= " AND $key = '$value'";
		}
		return $this->ejecutarConsulta($sql);
	}

	public function iniciarTransaccion(){
		$sql = "START TRANSACTION;";
		return $this->ejecutarConsulta($sql);
	}

	public function commitTransaccion(){
		$sql = "COMMIT;";
		return $this->ejecutarConsulta($sql);
	}

	public function rollbackTransaccion(){
		$sql = "ROLLBACK;";
		return $this->ejecutarConsulta($sql);
	}

	public function ejecutarConsulta($sql){
		$respuesta = [];
		$resultado = $this->query($sql);
		if($resultado === TRUE){
			$respuesta['ejecuto'] = true;
			$respuesta['insertId'] = $this->insert_id;
		}elseif(is_object($resultado)){
			$respuesta['ejecuto'] = true;
			$respuesta['data'] = [];
			while($row = $resultado->fetch_array(MYSQLI_ASSOC)){
				$respuesta['data'][] = $row;
			}
			$resultado->free();
		}else{
			$respuesta['ejecuto'] = false;
			$respuesta['codigoError'] = $this->errno;
			$respuesta['mensajeError'] = $this->error;
		}
		$this->close();
		return $respuesta;
	}

	public function obtenerDatosEnum($tabla, $datos){
		$arreglo = [];

		$sql = "SELECT 
					COLUMN_TYPE 
				FROM 
					INFORMATION_SCHEMA.COLUMNS 
				WHERE 
					TABLE_SCHEMA = '".$this->DB_NAME."' 
					AND TABLE_NAME = '".$tabla."' 
					AND COLUMN_NAME = '".$datos['columna']."'";
		$resultado = $this->ejecutarConsulta($sql);

		if($resultado['ejecuto']){
			if(sizeof($resultado['data']) > 0){
				$enumList = explode(",", str_replace("'", "", substr($resultado['data'][0]['COLUMN_TYPE'], 5, (strlen($resultado['data'][0]['COLUMN_TYPE'])-6))));

				foreach($enumList as $dato){
					$arreglo[] = ['id' => $dato, 'campo' => $dato];
				}

				sort($arreglo);
			}
		}

		return [
			'ejecuto' => $resultado['ejecuto'],
			'data' => $arreglo
		];
	}

	public function selectCompleto($tabla, $datos = [], $campos = '*', $join = [], $group_by = '', $order_by = '', $having = ''){
		$sql  = "SELECT ".$campos;
		$sql .= " FROM ".$tabla;

		//El join es un arreglo con 3 indices por ocurrencia (tipo de join, tabla a hacer join y la condicion)
		if(sizeof($join) > 0){
			foreach ($join as $value) {
				if(isset($value['tipo']) && isset($value['tabla']) && isset($value['condicion'])){
					$sql .= " ".$value['tipo']." JOIN ".$value['tabla']." ON ".$value['condicion'];
				}
			}
		}

		$sql .= " WHERE 1";

		if(sizeof($datos) > 0){
			foreach ($datos as $key => $value) {
				//Si es un arreglo, se usa el IN
				if(is_array($value)){
					$sql .= " AND $key IN ('".implode("', '", $value)."')";
				} else {
					$sql .= " AND $key = '$value'";
				}
			}
		}

		if($group_by != ""){
			$sql .= " GROUP BY ".$group_by;
		}

		if($order_by != ""){
			$sql .= " ORDER BY ".$order_by;
		}

		if($having != ""){
			$sql .= " HAVING ".$having;
		}

		return $this->ejecutarConsulta($sql);
	}
}