<?php

class baseCrud{
	protected $tabla;

	public function select($datos){
		$db = new database();
		return $db->select($this->tabla, $datos);
	}

	public function selectOtraTabla($tabla, $datos){
		$db = new database();
		return $db->select($tabla, $datos);
	}

	public function insert($datos){
		$db = new database();
		return $db->insert($this->tabla, $datos);
	}

	public function insertOtraTabla($tabla, $datos){
		$db = new database();
		return $db->insert($tabla, $datos);	
	}

	public function update($datos){
		$db = new database();
		return $db->update($this->tabla, $datos);
	}

	public function cantidad($datos){
		$db = new database();
		return $db->cantidad($this->tabla, $datos);
	}

	public function iniciarTransaccion(){
		$db = new database();
		return $db->iniciarTransaccion();
	}

	public function commitTransaccion(){
		$db = new database();
		return $db->commitTransaccion();
	}

	public function rollbackTransaccion(){
		$db = new database();
		return $db->rollbackTransaccion();
	}

	public function obtenerDatosEnum($datos){
		$db = new database();
		return $db->obtenerDatosEnum($this->tabla, $datos);
	}

	public function selectCompleto($tabla, $datos = [], $campos = '*', $join = [], $group_by = '', $order_by = '', $having = ''){
		$db = new database();
		return $db->selectCompleto($tabla, $datos, $campos, $join, $group_by, $order_by, $having);	
	}
}