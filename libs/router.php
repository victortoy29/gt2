<?php

class router {    
    public $api = false;
    public $controlador = 'main';
    public $metodo = 'index';
    public $parametros = array();
    public $path = [];

    public function __construct() {
        if(isset($_GET['url'])) {
            $this->url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);            
            $this->url = explode('/', $_GET['url']);
            $this->path = $this->url;
            if(!empty($this->url)) {
                //Se evalua si la primera palabra es API si no es asi, es porque es una vista
                if($this->url[0] == 'api') {
                    $this->api = true;
                    array_shift($this->url);
                }
                if(!empty($this->url)) {
                    $this->controlador = array_shift($this->url);
                    if(!empty($this->url)) {
                        $this->metodo = array_shift($this->url);
                        if(empty($this->metodo)) {
                            $this->metodo = "index";
                        }
                        if(!empty($this->url)) {
                            $this->parametros = $this->url;
                        }
                    }
                }
            }
        }
        $this->handle();
    }

    public function handle() {        
        try {
            if($this->api) {
                //Es una controlador
                session_start();
                require_once "controllers/".$this->controlador.".php";
                $objeto = new $this->controlador();
                echo json_encode($objeto->{$this->metodo}($_POST), JSON_NUMERIC_CHECK);
            } else {
                //Es una vista
                $ruta = "views/".$this->controlador."/".$this->metodo.".php";

                if(file_exists($ruta)){
                    require_once $ruta;
                } else {
                    throw new Exception("La página no existe");
                }
            }
        } catch (Exception $e) {
            if(sizeof($this->path) == 2){
                header("Location: ../main/error/404");
            } elseif (sizeof($this->path) == 3){
                header("Location: ../../main/error/404");
            }
        }
    }
}