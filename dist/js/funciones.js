function enviarPeticion(objeto, metodo, datos, callback){
	$.ajax({
		url:'api/'+objeto+'/'+metodo,
		type:'POST',
		dataType: 'json',					
		data: datos,
		success: function(respuesta){
			if(respuesta.ejecuto == true){
				callback(respuesta)
			}else{
				toastr.error(respuesta.mensajeError)
			}
		},
		error: function(xhr, status){
			console.log('Ocurrio un error', xhr.responseText, status)
			if(typeof(xhr.responseText) != 'undefined'){
				if(xhr.responseText != ''){
					toastr.error(xhr.responseText)
				}
			}
		}
	})
}

function enviarPeticionAsync(objeto, metodo, datos, callback){
	$.ajax({
		url:'api/'+objeto+'/'+metodo,
		type:'POST',
		dataType: 'json',					
		data: datos,
		async: false,
		success: function(respuesta){
			if(respuesta.ejecuto == true){
				callback(respuesta)
			}else{
				toastr.error(respuesta.mensajeError)
			}
		},
		error: function(xhr, status){
			console.log('Ocurrio un error', xhr.responseText, status)
			if(typeof(xhr.responseText) != 'undefined'){
				if(xhr.responseText != ''){
					toastr.error(xhr.responseText)
				}
			}
		}
	})
}

function parsearFormulario(form){
	let formulario = $(form).serializeArray()
	let respuesta = {}
	for(let i = 0; i < formulario.length; i++){
		respuesta[formulario[i].name] = formulario[i].value
	}
	return respuesta
}

function llenarSelect(objeto, metodo, datos, elemento, campo, defecto){
    enviarPeticion(objeto, metodo, datos, function(r){
        if(defecto == 1){
            $("#"+elemento).empty();
            $("#"+elemento).append("<option value=''>Seleccione...</option>");
        }        
        for (i = 0; i < r.data.length; i++){        
            $("#"+elemento).append("<option value="+r.data[i].id+">"+r.data[i][campo]+"</option>")
        }
    })
}

function llenarFormulario(formulario, objeto, metodo, datos, callback){
	enviarPeticion(objeto, metodo, datos, function(r){
		$.each(r.data[0], function(campo, valor){
			$('#'+formulario+' #'+campo).val(valor)
		})
		callback(r)
	})
}