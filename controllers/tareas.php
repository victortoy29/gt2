<?php
require_once "libs/database.php";
require_once "libs/baseCrud.php";

class tareas extends baseCrud{
	protected $tabla = 'tareas';

	public function insert($datos){
		$resultado = parent::insert($datos);

		if(!$resultado['ejecuto']){
			return [
				'ejecuto' => false,
				'mensajeError' => 'Se produjo un error al crear la tarea, por favor verifique los datos ingresados e intente de nuevo.'
			];
		}

		return $resultado;
	}

	public function selectJoin($datos){
		$resultado = parent::select($datos);

		if($resultado['ejecuto']){
			$datosHerramienta = ['id' => $resultado['data'][0]['fk_herramientas']];
			$resultadoHerr = parent::selectOtraTabla('herramientas', $datosHerramienta);

			if($resultadoHerr['ejecuto']){
				$datosResponsable = ['id' => $resultado['data'][0]['fk_usuarios']];
				$resultadoUsu = parent::selectOtraTabla('usuarios', $datosResponsable);

				if($resultadoUsu['ejecuto']){
					$resultado['data'][0]['herramienta'] = $resultadoHerr['data'][0];
					$resultado['data'][0]['usuario'] = $resultadoUsu['data'][0];
				}
			}
		} else {
			return [
				'ejecuto' => false,
				'mensajeError' => 'Se produjo un error al crear la tarea, por favor verifique los datos ingresados e intente de nuevo.'
			];
		}

		return $resultado;
	}
}