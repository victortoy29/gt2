<?php
require_once "libs/database.php";
require_once "libs/baseCrud.php";

class herramientas extends baseCrud{
	protected $tabla = 'herramientas';

	public function select($datos){
		$resultado = parent::select($datos);

		if($resultado['ejecuto']){
			if(sizeof($resultado['data']) > 0){
				$indice = 0;

				foreach($resultado['data'] as $herramienta){
					//Datos de la fase
					if($herramienta['fk_fasesproyecto'] > 0){
						$datosFase = ['id' => $herramienta['fk_fasesproyecto'], 'estado' => 'Activo'];
						$resultadoFase = parent::selectOtraTabla('fasesproyecto', $datosFase);
						$resultado['data'][$indice]['fase'] = null;
						
						if($resultadoFase['ejecuto']){
							$resultado['data'][$indice]['fase'] = [
								'codigo' => $resultadoFase['data'][0]['codigo'],
								'nombre' => $resultadoFase['data'][0]['nombre'],
								'descripcion' => $resultadoFase['data'][0]['descripcion']
							];
						}
					} else {
						$resultado['data'][$indice]['fase'] = [
							'codigo' => 'No Aplica',
							'nombre' => '',
							'descripcion' => ''
						];
					}
					

					$indice++;
				}
			}
		}

		return $resultado;
	}

	public function selectFase($datos){
		$resultadoHerr = parent::select(['fk_fasesproyecto' => 0]);
		$resultadoFase = parent::selectOtraTabla('fasesproyecto', $datos);

		$indice = 0;
		$resultado = [
			'ejecuto' => true,
			'data' => []
		];

		if($resultadoHerr['ejecuto']){
			if(sizeof($resultadoHerr['data']) > 0){
				foreach($resultadoHerr['data'] as $herramienta){
					$resultado['data'][$indice] = $herramienta;
					$resultado['data'][$indice]['fase'] = [
						'codigo' => 'No Aplica',
						'nombre' => '',
						'descripcion' => ''
					];

					$indice++;
				}
			}
		}

		if($resultadoFase['ejecuto']){
			if(sizeof($resultadoFase['data']) > 0){
				foreach($resultadoFase['data'] as $fase){
					$resultadoHerrFase = parent::select(['fk_fasesproyecto' => $fase['id']]);

					if($resultadoHerrFase['ejecuto']){
						if(sizeof($resultadoHerrFase['data']) > 0){
							foreach($resultadoHerrFase['data'] as $herramienta){
								$resultado['data'][$indice] = $herramienta;
								$resultado['data'][$indice]['fase'] = [
									'codigo' => $fase['codigo'],
									'nombre' => $fase['nombre'],
									'descripcion' => $fase['descripcion']
								];

								$indice++;
							}
						}
					}
				}
			}
		}

		return $resultado;
	}
}