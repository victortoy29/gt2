<?php
require_once "libs/database.php";
require_once "libs/baseCrud.php";

class proyectos extends baseCrud{
	protected $tabla = 'proyectos';
	
	public function insert($datos){
		//Se usa transaccion para que solo ingrese todo cuando este bien
		$respuestaTr = parent::iniciarTransaccion();

		if($respuestaTr['ejecuto']){
			$respuesta = parent::insert($datos);

			if($respuesta['ejecuto']){
				//Se inserta tambien en la tabla de integrantes con el rol de Lider
				$datosIntegrante = [
					'rol' => 'Lider',
                    'estado' => 'Activo',
                    'fk_usuarios' => $_SESSION['usuario']['id'],
                    'fk_proyectos' => $respuesta['insertId']
				];
				$respuestaIn = parent::insertOtraTabla('integrantes', $datosIntegrante);

				if($respuestaIn['ejecuto']){
					parent::commitTransaccion();
					return $respuesta;
				} else {
					parent::rollbackTransaccion();
					return $respuestaIn;
				}
			} else {
				parent::rollbackTransaccion();
				return $respuesta;
			}
		} else {
			parent::rollbackTransaccion();
			return $respuestaTr;
		}
	}

	public function select($datos){
		$resultado = parent::select($datos);

		if($resultado['ejecuto']){
			if(sizeof($resultado['data']) > 0){
				$indice = 0;

				foreach($resultado['data'] as $proyecto){
					//Datos de los integrantes
					$datosIntegrantes = ['fk_proyectos' => $proyecto['id']];
					$resultadoIn = parent::selectOtraTabla('integrantes', $datosIntegrantes);

					//Datos del creador
					$datosCreador = ['id' => $proyecto['creado_por']];
					$resultadoCr = parent::selectOtraTabla('usuarios', $datosCreador);

					$resultado['data'][$indice]['cantidad_integrantes'] = 0;
					$resultado['data'][$indice]['listado_integrantes'] = [];
					$resultado['data'][$indice]['creador'] = '';

					if($resultadoIn['ejecuto']){
						foreach($resultadoIn['data'] as $integrante){
							$resultado['data'][$indice]['listado_integrantes'][] = $integrante;
							
							if($integrante['estado'] == 'Activo'){
								//Solo se tiene en cuenta al integrante si esta activo
								$resultado['data'][$indice]['cantidad_integrantes'] = $resultado['data'][$indice]['cantidad_integrantes'] + 1;
							}
						}
					}

					if($resultadoCr['ejecuto']){
						if(sizeof($resultadoCr['data']) > 0){
							$resultado['data'][$indice]['creador'] = $resultadoCr['data'][0]['nombres'].' '.$resultadoCr['data'][0]['apellidos'];
						}
					}

					//Datos de la fase
					if($proyecto['fk_fasesproyecto'] > 0){
						$datosFase = ['id' => $proyecto['fk_fasesproyecto']];
						$resultadoFase = parent::selectOtraTabla('fasesproyecto', $datosFase);
						$resultado['data'][$indice]['fase'] = null;
						
						if($resultadoFase['ejecuto']){
							$resultado['data'][$indice]['fase'] = [
								'codigo' => $resultadoFase['data'][0]['codigo'],
								'nombre' => $resultadoFase['data'][0]['nombre'],
								'descripcion' => $resultadoFase['data'][0]['descripcion']
							];
						}
					} else {
						$resultado['data'][$indice]['fase'] = [
							'codigo' => 'Pendiente',
							'nombre' => 'Se debe cambiar la fase',
							'descripcion' => ''
						];
					}

					//Datos del tipo
					$datosTipo = ['id' => $proyecto['fk_tiposproyecto'], 'estado' => 'Activo'];
					$resultadoTipo = parent::selectOtraTabla('tiposproyecto', $datosTipo);
					$resultado['data'][$indice]['tipo'] = null;
					
					if($resultadoTipo['ejecuto']){
						$resultado['data'][$indice]['tipo'] = [
							'nombre' => $resultadoTipo['data'][0]['nombre'],
							'descripcion' => $resultadoTipo['data'][0]['descripcion']
						];
					}

					$indice++;
				}
			}
		}

		return $resultado;
	}

	public function updateFase($datos){
		//Se usa transaccion para que solo ingrese todo cuando este bien
		$respuestaTr = parent::iniciarTransaccion();

		if($respuestaTr['ejecuto']){
			$resultado = [
				'ejecuto' => false,
				'mensajeError' => 'Se produjo un error al actualizar el proyecto, por favor verifique los datos ingresados e intente de nuevo.'
			];

			$resultadoSel = parent::select(['id' => $datos['id']]);

			if($resultadoSel['ejecuto']){
				if(sizeof($resultadoSel['data']) > 0){
					//Se crean los datos del cambio de fase
					$datosCambio = [
						'fk_proyectos' => $datos['id'],
					    'fk_fasesproyecto_inicial' => $resultadoSel['data'][0]['fk_fasesproyecto'],
					    'fk_fasesproyecto_final' => $datos['fk_fasesproyecto']
					];

					$resultadoIn = parent::insertOtraTabla('cambiofaseproyecto', $datosCambio);

					if($resultadoIn['ejecuto']){
						$resultado = parent::update($datos);
					}
				}
			}

			if($resultado['ejecuto']){
				parent::commitTransaccion();
			} else {
				parent::rollbackTransaccion();
			}

			return $resultado;
		} else {
			parent::rollbackTransaccion();
			return $respuestaTr;
		}
	}

	public function selectReporteProyectosTipoFase(){
		$tabla = $this->tabla.' p';
		$datos = [];
		$campos = 'COUNT(*) AS cantidad, p.fk_tiposproyecto, p.fk_fasesproyecto, t.nombre AS tipo, CONCAT(f.codigo, " - ", f.nombre) AS fase';
		$join = [
			['tipo' => 'INNER', 'tabla' => 'tiposproyecto t', 'condicion' => 'p.fk_tiposproyecto = t.id'],
			['tipo' => 'INNER', 'tabla' => 'fasesproyecto f', 'condicion' => 'p.fk_fasesproyecto = f.id']
		];
		$group_by = 'p.fk_tiposproyecto, p.fk_fasesproyecto';
		$resultado = parent::selectCompleto($tabla, $datos, $campos, $join, $group_by);
		
		return $resultado;
	}

	public function selectReporteProyectosTipo(){
		$tabla = $this->tabla.' p';
		$datos = [];
		$campos = 'COUNT(*) AS cantidad, p.fk_tiposproyecto, t.nombre AS tipo';
		$join = [
			['tipo' => 'INNER', 'tabla' => 'tiposproyecto t', 'condicion' => 'p.fk_tiposproyecto = t.id']
		];
		$group_by = 'p.fk_tiposproyecto';
		$resultado = parent::selectCompleto($tabla, $datos, $campos, $join, $group_by);
		
		return $resultado;
	}
}
