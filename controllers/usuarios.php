<?php
require_once "libs/database.php";
require_once "libs/baseCrud.php";

class usuarios extends baseCrud{
	protected $tabla = 'usuarios';

	public function insert($datos){
		//Se usa database porque no hay sesion activa
		$db = new database();

		if(isset($datos['password'])){
			if($datos['password'] != ""){
				$datos['password'] = md5($datos['password']);
			}
		}

		if(!isset($datos['rol'])){
			$datos['rol'] = 'Usuario';
		}

		if(!isset($datos['habilitado_proyectos'])){
			$datos['habilitado_proyectos'] = 'NO';
		}

		$sql = "INSERT INTO usuarios SET ";				
		foreach ($datos as $key => $value) {
			$sql .= "$key = '$value',";
		}
		$sql .= "creado_por = 1, fecha_creacion = NOW()";
		$resultado = $db->ejecutarConsulta($sql);
		
		if($resultado['ejecuto']){
			$usuario = [
				'id' => $resultado['insertId'],
				'nombres' => $datos['nombres'],
				'apellidos' => $datos['apellidos'],
				'correo' => $datos['correo'],
				'numero_contacto' => $datos['numero_contacto'],
				'rol' => $datos['rol'],
				'habilitado_proyectos' => $datos['habilitado_proyectos']
			];

			$_SESSION['usuario'] = $usuario;

			return [
				'ejecuto' => true,
				'data' => $_SESSION
			];
		} else {
			if($resultado['codigoError'] == 1062){
				return [
					'ejecuto' => false,
					'mensajeError' => 'El usuario ya se encuentra registrado en el sistema.'
				];
			} else {
				return $resultado;	
			}
		}
	}

	public function insertAdmin($datos){
		if(isset($datos['password'])){
			if($datos['password'] != ""){
				$datos['password'] = md5($datos['password']);
			}
		}
		
		$resultado = parent::insert($datos);
		return $resultado;
	}

	public function ingreso($datos){
		$datos['password'] = md5($datos['password']);
		$resultado = parent::select($datos);
		
		if(count($resultado['data']) == 0) {
			return [
				'ejecuto' => false,
				'mensajeError' => 'Credenciales erróneas'
			];
		} elseif($resultado['data'][0]['estado'] == 'Inactivo'){
			return [
				'ejecuto' => false,
				'mensajeError' => 'Su usuario ha sido desactivado'
			];
		} else {
			$usuario = [
				'id' => $resultado['data'][0]['id'],
				'nombres' => $resultado['data'][0]['nombres'],
				'apellidos' => $resultado['data'][0]['apellidos'],
				'correo' => $resultado['data'][0]['correo'],
				'numero_contacto' => $resultado['data'][0]['numero_contacto'],
				'rol' => $resultado['data'][0]['rol'],
				'habilitado_proyectos' => $resultado['data'][0]['habilitado_proyectos']
			];

			$_SESSION['usuario'] = $usuario;
		}

		return [
			'ejecuto' => true,
			'data' => $_SESSION
		];
	}

	public function update($datos){
		if(isset($datos['password'])){
			$datos['password'] = md5($datos['password']);
		}
		
		$resultado = parent::update($datos);
		
		if($resultado['ejecuto']){
			$datosIngreso = ['id' => $datos['id']];
			$resultado = parent::select($datosIngreso);
		
			if($resultado['ejecuto']){
				$usuario = [
					'id' => $resultado['data'][0]['id'],
					'nombres' => $resultado['data'][0]['nombres'],
					'apellidos' => $resultado['data'][0]['apellidos'],
					'correo' => $resultado['data'][0]['correo'],
					'numero_contacto' => $resultado['data'][0]['numero_contacto'],
					'rol' => $resultado['data'][0]['rol'],
					'habilitado_proyectos' => $resultado['data'][0]['habilitado_proyectos']
				];

				$_SESSION['usuario'] = $usuario;

				return [
					'ejecuto' => true,
					'data' => $usuario
				];
			} else {
				return $resultado;
			}
		} else {
			return $resultado;
		}
	}

	public function updateAdmin($datos){
		if(isset($datos['password'])){
			$datos['password'] = md5($datos['password']);
		}
		
		$resultado = parent::update($datos);
		return $resultado;
	}

	public function exportar($datos){
		//Exportar usuarios en excel
		header("Content-Type: application/vnd.ms-excel; charset=utf-8");
		header("Content-type: application/x-msexcel; charset=utf-8");
		header('Content-disposition: attachment; filename=ReporteUsuariosPlataforma_'.date('Ymd_His').'.xls');
		@session_start();
		$tabla = '<!DOCTYPE html>
			<html lang="es">
			<head>
			    <meta charset="utf-8">
		    </head>
			<body>
				<table border="1">';
		
		try {
			if(isset($_SESSION['usuario'])){
				if($_SESSION['usuario']['rol'] == 'Administrador'){
					$datosIngreso = ['rol' => 'Usuario'];
					$resultado = parent::select($datosIngreso);
				
					if($resultado['ejecuto']){
						if(sizeof($resultado['data']) > 0){
							$tabla .= '<tr>
									<th style="background-color: #DADADA;">Correo Electrónico</th>
									<th style="background-color: #DADADA;">Nombres</th>
									<th style="background-color: #DADADA;">Apellidos</th>
									<th style="background-color: #DADADA;">Número de Contacto</th>
									<th style="background-color: #DADADA;">Interés en la Plataforma</th>
									<th style="background-color: #DADADA;">Descripción del Emprendimiento</th>
									<th style="background-color: #DADADA;">Estado</th>
									<th style="background-color: #DADADA;">Habilitado para Proyectos</th>
								</tr>';

							foreach($resultado['data'] as $usuario){
								$tabla .= '<tr>
										<td>'.$usuario['correo'].'</td>
										<td>'.$usuario['nombres'].'</td>
										<td>'.$usuario['apellidos'].'</td>
										<td>'.$usuario['numero_contacto'].'</td>
										<td>'.$usuario['interes'].'</td>
										<td>'.$usuario['descripcion'].'</td>
										<td>'.$usuario['estado'].'</td>
										<td>'.$usuario['habilitado_proyectos'].'</td>
									</tr>';
							}
						} else {
        					$tabla .= '<tr><td>No hay usuarios para exportar.</td></tr>';
						}
					} else {
        				$tabla .= '<tr><td>Error al exportar los datos de la tabla, por favor comuníquese con el administrador del sistema.</td></tr>';
					}
				}
			}
        } catch (Exception $e) {
        	$tabla .= '<tr><td>Error al exportar los datos, por favor comuníquese con el administrador del sistema.</td></tr>';
        }

		$tabla .= '</table>
			</body>
		</html>';

        echo $tabla;
		exit;
	}
}