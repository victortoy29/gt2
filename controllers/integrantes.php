<?php
require_once "libs/database.php";
require_once "libs/baseCrud.php";

class integrantes extends baseCrud{
	protected $tabla = 'integrantes';

	public function insert($datos){
		$datos['rol'] = 'Miembro';
		$datos['estado'] = 'Pendiente';
		$datos['fk_usuarios'] = $_SESSION['usuario']['id'];

		$resultado = parent::insert($datos);

		if(!$resultado['ejecuto'] && $resultado['codigoError'] == 1062){
			$resultado['mensajeError'] = 'El usuario ya se encuentra registrado en este proyecto';
		}
		
		return $resultado;
	}

	public function insertAdmin($datos){
		$resultado = parent::insert($datos);

		if(!$resultado['ejecuto'] && $resultado['codigoError'] == 1062){
			$resultado['mensajeError'] = 'El usuario ya se encuentra registrado en este proyecto';
		}

		return $resultado;
	}

	public function select($datos){
		$resultado = parent::select($datos);

		if($resultado['ejecuto']){
			if(sizeof($resultado['data']) > 0){
				$indice = 0;

				foreach($resultado['data'] as $integrante){
					//Datos del usuario
					$datosUsuario = ['id' => $integrante['fk_usuarios']];
					$resultadoUsu = parent::selectOtraTabla('usuarios', $datosUsuario);
					$resultado['data'][$indice]['usuario'] = null;
					
					if($resultadoUsu['ejecuto']){
						$resultado['data'][$indice]['usuario'] = [
							'id' => $resultadoUsu['data'][0]['id'],
							'nombres' => $resultadoUsu['data'][0]['nombres'],
							'apellidos' => $resultadoUsu['data'][0]['apellidos'],
							'rol' => $resultadoUsu['data'][0]['rol'],
							'estado' => $resultadoUsu['data'][0]['estado'],
						];
					}

					$indice++;
				}
			}
		}

		return $resultado;
	}
}

