<?php require('views/header.php'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Listado de Usuarios</h1>
                    </div>
                    <div class="col-sm-6">
                        <div class="float-sm-right">
                            <div class="btn-group" role="group">
                                <button type="button" id="crearUsuario" class="btn btn-success">Nuevo</button>
                                <a href="api/usuarios/exportar" class="btn btn-primary"><i class="fas fa-download"></i> Exportar en Excel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="tabla_usuarios" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr class="text-center">
                                            <th>ID</th>
                                            <th>Correo</th>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                            <th>Número Contacto</th>
                                            <th>Rol</th>
                                            <th>Habilitado Proyectos</th>
                                            <th>Estado</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="modalUsuarios" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 id="tituloModal" class="modal-title">Datos del Usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formularioUsuario">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="correo">Correo electrónico</label>
                                    <input type="email" class="form-control" name="correo" id="correo" placeholder="Correo electrónico" disabled="disabled" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="nombres">Nombre(s)</label>
                                    <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombre(s)" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="numero_contacto">Número de contacto</label>
                                    <input type="number" class="form-control" name="numero_contacto" id="numero_contacto" placeholder="Número de contacto" required="required" min="0">
                                </div>
                                <div class="form-group">
                                    <label class="form-check-label" for="habilitado_proyectos">Puede crear proyectos de emprendimiento</label>
                                    <select class="form-control" name="habilitado_proyectos" id="habilitado_proyectos" required="required"></select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="apellidos">Apellido(s)</label>
                                    <input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellido(s)" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="rol">Perfil</label>
                                    <select class="form-control" name="rol" id="rol" required="required"></select>
                                </div>
                                <div class="form-group">
                                    <label class="form-check-label" for="estado">Estado</label>
                                    <select class="form-control" name="estado" id="estado" required="required"></select>
                                </div>
                            </div>
                        </div>
                        <div id="fila-nota" class="row display-none">
                            <div class="col-12">
                                <div class="alert alert-info">
                                    <b>NOTA:</b> Solo llena este campo si deseas cambiar la contraseña.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="password">Contraseña</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-submit" id="botonCrearUsuario" form="formularioUsuario">Crear Usuario</button>
                    <button type="submit" class="btn btn-primary btn-submit" id="botonActualizarUsuario" form="formularioUsuario">Actualizar Usuario</button>
                </div>
            </div>
        </div>
    </div>

<?php require('views/footer.php');?>
<script type="text/javascript">
    let id
    let idUser
    let habilitadoCrear
    let tabla = null

    function init(info){
        let boton

        enviarPeticion('usuarios', 'obtenerDatosEnum', {columna: 'rol'}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){
                        $('#rol').append('<option value="'+r.data[i].id+'">'+r.data[i].campo+'</option>')
                    }
                }
            }
        })

        enviarPeticion('usuarios', 'obtenerDatosEnum', {columna: 'habilitado_proyectos'}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){
                        $('#habilitado_proyectos').append('<option value="'+r.data[i].id+'">'+r.data[i].campo+'</option>')
                    }
                }
            }
        })

        enviarPeticion('usuarios', 'obtenerDatosEnum', {columna: 'estado'}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){
                        $('#estado').append('<option value="'+r.data[i].id+'">'+r.data[i].campo+'</option>')
                    }
                }
            }
        })

        if(info.data.length == 0){
            window.location.href = 'main/index'
        } else {
            idUser = info.data.usuario.id

            if(info.data.usuario.rol != 'Administrador'){
                window.location.href = 'main/index'
            }
        }
        
        cargarRegistros({}, 'crear', function(){})

        //Creacion de nuevo usuario
        $('#crearUsuario').on('click', function(){
            $('#botonCrearUsuario').show()
            $('#botonActualizarUsuario').hide()
            $('#correo').val('').attr('disabled', false)
            $('#nombres').val('')
            $('#apellidos').val('')
            $('#rol').val('Usuario')
            $('#numero_contacto').val('')
            $('#estado').val('Activo')
            $('#habilitado_proyectos').val('NO')
            $('#password').val('').attr('required', true)
            $('#fila-nota').hide()
            $('#modalUsuarios').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioUsuario').on('submit', function(e){
            e.preventDefault()
            let datos = parsearFormulario($(this))
            
            if(boton == 'botonCrearUsuario'){
                enviarPeticion('usuarios', 'insertAdmin', datos, function(r){
                    if(r.ejecuto == true){
                        cargarRegistros({id: r.insertId}, 'crear', function(){
                            $('#modalUsuarios').modal('hide')
                            $('#correo').val('').attr('disabled', true)
                            $('#nombres').val('')
                            $('#apellidos').val('')
                            $('#rol').val('Usuario')
                            $('#numero_contacto').val('')
                            $('#estado').val('Activo')
                            $('#habilitado_proyectos').val('NO')
                            $('#password').val('').attr('required', false)
                        })
                    } else {
                        toastr.error('Se produjo un error al crear el usuario, por favor verifique los datos ingresados e intente de nuevo')
                    }
                })
            } else {
                datos.id = id

                if($.trim($('#password').val()) == ''){
                    delete datos.password
                }
                
                enviarPeticion('usuarios', 'updateAdmin', datos, function(r){
                    if(r.ejecuto == true){
                        cargarRegistros({id: id}, 'actualizar', function(){
                            $('#modalUsuarios').modal('hide')
                            $('#correo').val('').attr('disabled', true)
                            $('#nombres').val('')
                            $('#apellidos').val('')
                            $('#rol').val('Usuario')
                            $('#numero_contacto').val('')
                            $('#estado').val('Activo')
                            $('#habilitado_proyectos').val('NO')
                            $('#password').val('').attr('required', false)
                        })
                    } else {
                        toastr.error('Se produjo un error al actualizar el usuario, por favor verifique los datos ingresados e intente de nuevo')
                    }
                })
            }
        })
    }
    
    function cargarRegistros(datos, accion, callback){
        //Informacion de los usuarios
        enviarPeticion('usuarios', 'select', datos, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    let fila = ''
                    
                    for(let i = 0; i < r.data.length; i++){
                        let etiqueta = 'info'
                        let etiquetaEstado = 'success'

                        if(r.data[i].estado == 'Activo'){
                            botonEstado = '<button class="btn btn-default btn-sm text-red" onClick="desactivarUsuario(' + r.data[i].id + ')"><i class="fas fa-trash"></i></button>'
                            etiquetaEstado = 'success'
                        } else {
                            botonEstado = '<button class="btn btn-default btn-sm text-green" onClick="activarUsuario(' + r.data[i].id + ')"><i class="fas fa-check"></i></button>'

                            if(r.data[i].estado == 'Pendiente'){
                                etiquetaEstado = 'warning'
                            } else {
                                etiquetaEstado = 'danger'
                            }
                        }

                        if(r.data[i].id == '1'){
                            botonEstado = ''
                        }

                        if(r.data[i].rol == 'Administrador'){
                            etiqueta = 'success'
                        }
                        else if(r.data[i].rol == 'Mentor' || r.data[i].rol == 'Asesor'){
                            etiqueta = 'info'
                        }

                        fila += '<tr id="usuario'+r.data[i].id+'">'+
                            '<td align="center">'+r.data[i].id+'</td>'+
                            '<td>'+r.data[i].correo+'</td>'+
                            '<td>'+r.data[i].nombres+'</td>'+
                            '<td>'+r.data[i].apellidos+'</td>'+
                            '<td>'+r.data[i].numero_contacto+'</td>'+
                            '<td align="center"><div class="badge badge-'+etiqueta+'">'+r.data[i].rol+'</div></td>'+
                            '<td align="center">'+r.data[i].habilitado_proyectos+'</td>'+
                            '<td align="center"><div class="badge badge-'+etiquetaEstado+'">'+r.data[i].estado+'</div></td>'+
                            '<td>'+                                
                                '<button class="btn btn-default btn-sm" onClick="editarUsuario('+r.data[i].id+')"><i class="fas fa-edit"></i></button> '+
                                    botonEstado+
                                
                            '</td>'+
                        '</tr>'
                    }

                    //Datos para reconstruir la tabla
                    let pagina = 0
                    let filtro = ''
                    let orden = [0, 'asc']

                    if(tabla != null){
                        //Si la tabla existe, se destruye para que se pueda crear nuevamente
                        pagina = tabla.page()
                        filtro = tabla.search()
                        orden = [tabla.order()[0][0], tabla.order()[0][1]]
                        tabla.destroy()
                    }

                    //Se agrega o actualiza la fila dependiendo de la accion
                    if(accion == 'crear'){
                        $('#tabla_usuarios tbody').append(fila)
                    } else {
                        $('#usuario'+r.data[0].id).replaceWith(fila)
                    }

                    tabla = $('#tabla_usuarios').DataTable({
                        'paging': true,
                        'lengthChange': true,
                        'searching': true,
                        'ordering': true,
                        'language': {
                            'lengthMenu': 'Mostrar _MENU_ registros por página',
                            'zeroRecords': 'No hay datos',
                            'info': 'Página _PAGE_ de _PAGES_',
                            'infoEmpty': 'No hay datos disponibles',
                            'infoFiltered': '(filtrado de _MAX_ registros totales)',
                            'search': 'Buscar',
                            'paginate': {'first': 'Primero',
                                        'last': 'Último',
                                        'next': 'Siguiente',
                                        'previous': 'Anterior'
                            }
                        }
                    })

                    if(filtro != ''){
                        tabla.search(filtro).draw(false)
                    }

                    tabla.page(pagina).order([orden]).draw(false)
                }
            }

            callback()
        })
    }

    function editarUsuario(idUsuario){
        id = idUsuario

        enviarPeticion('usuarios', 'select', {id: idUsuario}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    $.each(r.data[0], function(campo, valor){
                        $('#'+campo).val(valor)
                    })

                    $('#correo').attr('disabled', true)
                    $('#password').val('').attr('required', false)
                    $('#botonCrearUsuario').hide()
                    $('#botonActualizarUsuario').show()
                    $('#fila-nota').show()
                    $('#modalUsuarios').modal('show')
                }
            }
        })
    }

    function activarUsuario(idUsuario){
        id = idUsuario

        if(confirm('Realmente desea activar el usuario?')){
            datos = {}
            datos.id = idUsuario
            datos.estado = 'Activo'

            enviarPeticion('usuarios', 'updateAdmin', datos, function(r){
                if(r.ejecuto == true){
                    cargarRegistros({id: idUsuario}, 'actualizar', function(){})
                } else {
                    toastr.error('Se produjo un error al activar el usuario, por favor verifique los datos ingresados e intente de nuevo')
                }
            })
        }
    }

    function desactivarUsuario(idUsuario){
        id = idUsuario

        if(confirm('Realmente desea desactivar el usuario?')){
            datos = {}
            datos.id = idUsuario
            datos.estado = 'Inactivo'

            enviarPeticion('usuarios', 'updateAdmin', datos, function(r){
                if(r.ejecuto == true){
                    cargarRegistros({id: idUsuario}, 'actualizar', function(){})
                } else {
                    toastr.error('Se produjo un error al desactivar el usuario, por favor verifique los datos ingresados e intente de nuevo')
                }
            })
        }
    }
</script>
</body>
</html>