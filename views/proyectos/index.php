<?php require('views/header.php'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 id="tituloPagina">Listado de Proyectos</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        <a href="proyectos/index" class="btn btn-primary">Ver Todos los Proyectos</a>
                        <a href="proyectos/index/1" id="btnProyectos" class="btn btn-info">Ir a Mis Proyectos</a>
                        <button type="button" id="crearProyecto" class="btn btn-success display-none">Crear Proyecto</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row" id="contenido"></div>
        </div>
    </section>
</div>

<div id="modalProyecto" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 id="tituloModal" class="modal-title">Crear Proyecto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formularioProyecto">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required="required">
                    </div>
                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <textarea class="form-control" id="descripcion" name="descripcion" rows="4" placeholder="Descripción del proyecto" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="fk_tiposproyecto">Tipo de Proyecto</label>
                        <select class="form-control" name="fk_tiposproyecto" id="fk_tiposproyecto" required="required">
                            <option value="">Seleccione uno</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-submit" id="botonCrearProyecto" form="formularioProyecto">Crear</button>
                <button type="submit" class="btn btn-secondary btn-submit" id="botonActualizarProyecto" form="formularioProyecto">Actualizar</button>
            </div>
        </div>
    </div>
</div>

<?php require('views/footer.php');?>
<script type="text/javascript">
    let propio = '<?php echo (isset($this->parametros) && sizeof($this->parametros) > 0) ? $this->parametros[0] : 0; ?>'
    let id
    let idUser
    let habilitadoCrear
    let rol

    function init(info){
        if(info.data.length == 0){
            idUser = 0
            habilitadoCrear = 'NO'
        } else {
            idUser = info.data.usuario.id
            habilitadoCrear = info.data.usuario.habilitado_proyectos
            rol = info.data.usuario.rol

            //Se muestra el boton de crear proyectos
            if(habilitadoCrear == 'SI'){
                $("#crearProyecto").show()
            }
        }
        
        //Se determina si se cargan los proyectos propios o todos
        if(propio == '1'){
            $('#tituloPagina').html('Mis Proyectos')
            cargarRegistros({creado_por: info.data.usuario.id}, 'crear', function(){})
        } else {
            cargarRegistros({estado: ['Pendiente', 'En Curso']}, 'crear', function(){})
        }

        if(rol == 'Administrador'){
            $('#btnProyectos').addClass('display-none')
        }
        
        //Creacion de nuevo proyecto
        $('#crearProyecto').on('click', function(){
            $('#nombre').val('')
            $('#descripcion').val('')
            $('#fk_tiposproyecto').val('')
            $('#botonCrearProyecto').show()
            $('#botonActualizarProyecto').hide()
            $('#tituloModal').html('Crear Proyecto')
            $('#modalProyecto').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })
        
        $('#formularioProyecto').on('submit', function(e){
            e.preventDefault()
            let datos = parsearFormulario($(this))
            
            if(boton == 'botonCrearProyecto'){
                enviarPeticion('proyectos', 'insert', datos, function(r){
                    if(r.ejecuto == true){
                        cargarRegistros({id: r.insertId}, 'crear', function(){
                            $('#modalProyecto').modal('hide')
                            $('#nombre').val('')
                            $('#descripcion').val('')
                            $('#fk_tiposproyecto').val('')

                            if($('#filaNoProyectos').length > 0){
                                $('#filaNoProyectos').remove()
                            }
                        })
                    } else {
                        toastr.error('Se produjo un error al crear el proyecto, por favor verifique los datos ingresados e intente de nuevo')
                    }
                })
            } else {
                datos.id = id
                
                enviarPeticion('proyectos', 'update', datos, function(r){
                    if(r.ejecuto == true){
                        cargarRegistros({id: id}, 'actualizar', function(){
                            $('#modalProyecto').modal('hide')
                            $('#nombre').val('')
                            $('#descripcion').val('')
                            $('#fk_tiposproyecto').val('')
                        })
                    } else {
                        toastr.error('Se produjo un error al crear el proyecto, por favor verifique los datos ingresados e intente de nuevo')
                    }
                })
            }
        })

        enviarPeticion('tiposproyecto', 'select', {estado: 'Activo'}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){
                        $('#fk_tiposproyecto').append('<option value="'+r.data[i].id+'">'+r.data[i].nombre+'</option>')
                    }
                }
            }
        })
    }
    
    function cargarRegistros(datos, accion, callback){
        //Informacion de los proyectos
        enviarPeticion('proyectos', 'select', datos, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    let fila = ''
                    
                    for(let i = 0; i < r.data.length; i++){
                        let etiqueta = 'primary'
                        let botonera = ''
                        let estado = r.data[i].fase.codigo
                        let botonVer = '<a class="btn btn-default btn-sm" href="proyectos/detalles/'+r.data[i].id+'" alt="Ver" title="Ver"><i class="fas fa-eye"></i></a>&nbsp;'
                        let botonEditar = '<button class="btn btn-default btn-sm" onClick="editarProyecto(' + r.data[i].id + ')" alt="Editar" title="Editar"><i class="fas fa-edit"></i></button>&nbsp;'
                        let botonBorrar = '<button class="btn btn-default btn-sm text-red" onClick="desactivarProyecto('+r.data[i].id+')" alt="Borrar" title="Borrar"><i class="fas fa-trash"></i></button>&nbsp;'
                        let botonUnirse = '<button class="btn btn-info btn-sm" onClick="unirseProyecto('+r.data[i].id+')" alt="Unirse al Proyecto" title="Unirse al Proyecto"><i class="fas fa-sign-in-alt"></i></button>'
                        let botonUnirseDeshabilitado = '<button class="btn btn-primary btn-sm" alt="Debe esperar la aprobación del líder del proyecto" title="Debe esperar la aprobación del líder del proyecto" disabled="disabled"><i class="fas fa-sign-in-alt"></i></button>'
                        let botonUnirseRechazado = '<button class="btn btn-danger btn-sm" alt="Ha sido rechazado para la participación en este proyecto" title="Ha sido rechazado para la participación en este proyecto" disabled="disabled"><i class="fas fa-sign-in-alt"></i></button>'
                        
                        if(idUser != 0){
                            if(idUser == r.data[i].creado_por){
                                botonera = botonVer + botonEditar
                            } else {
                                for(let j = 0; j < r.data[i].listado_integrantes.length; j++){
                                    if(idUser == r.data[i].listado_integrantes[j].fk_usuarios && r.data[i].listado_integrantes[j].estado == 'Activo'){
                                        botonera = botonVer
                                    }
                                }
                            }

                            if(botonera == '' && habilitadoCrear == 'SI'){
                                for(let j = 0; j < r.data[i].listado_integrantes.length; j++){
                                    if(idUser == r.data[i].listado_integrantes[j].fk_usuarios && r.data[i].listado_integrantes[j].estado == 'Pendiente'){
                                        botonera = botonUnirseDeshabilitado
                                    }

                                    if(idUser == r.data[i].listado_integrantes[j].fk_usuarios && (r.data[i].listado_integrantes[j].estado == 'Rechazado' || r.data[i].listado_integrantes[j].estado == 'Inactivo')){
                                        botonera = botonUnirseRechazado
                                    }
                                }

                                if(botonera == ''){
                                    botonera = botonUnirse
                                }
                            }
                        } else {
                            botonera = ''
                        }

                        if(rol == 'Administrador'){
                            botonera = botonVer + botonEditar

                            if(r.data[i].estado != 'Finalizado'){
                                botonera += botonBorrar
                            }
                        }

                        if(r.data[i].estado == 'Finalizado'){
                            etiqueta = 'success'
                        }
                        else if(r.data[i].estado == 'Cancelado' || r.data[i].estado == 'Rechazado'){
                            botonera = ''
                            etiqueta = 'danger'
                        }
                        else if(r.data[i].estado == 'Pendiente'){
                            etiqueta = 'warning'
                        } 

                        if(r.data[i].fk_fasesproyecto == 1){
                            etiqueta = 'warning'
                        } else {
                            etiqueta = 'primary'
                        }

                        let fecha = new Date(r.data[i].fecha_creacion)
                        let anho = new Intl.DateTimeFormat('es-CO', { year: 'numeric' }).format(fecha)
                        let mes = new Intl.DateTimeFormat('es-CO', { month: 'short' }).format(fecha)
                        let dia = new Intl.DateTimeFormat('es-CO', { day: '2-digit' }).format(fecha)
                        let fecha_creacion = dia + ' ' + mes + ' ' + anho

                        fila += '<div class="col-lg-4" id="proyecto'+r.data[i].id+'">'+
                            '<div class="card card-outline card-'+etiqueta+'">'+
                                '<div class="card-header border-bottom-0">'+
                                    '<div class="row">'+
                                        '<div class="ribbon ribbon-'+etiqueta+'"><span>'+estado+'</span></div>'+
                                        '<div class="col-12">'+
                                            '<h3 class="card-title"><b>'+r.data[i].nombre+'</b></h3>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="card-body pt-0">'+
                                    '<div class="row">'+
                                        '<div class="col-7">'+
                                            '<p class="text-sm">'+r.data[i].descripcion+'</p>'+
                                        '</div>'+
                                        '<div class="col-5 text-center">'+
                                            '<img src="dist/img/grupo.png" alt="" class="img-circle img-fluid" />'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row">'+
                                        '<div class="col-12 text-muted text-xs">'+r.data[i].tipo.nombre+'<br /><br /></div>'+
                                    '</div>'+
                                    '<div class="row">'+
                                        '<div class="col-1 text-muted text-xs" alt="Cantidad de Miembros" title="Cantidad de Miembros"><i class="fas fa-users"></i> <span id="cantidad_'+r.data[i].id+'">'+r.data[i].cantidad_integrantes+'</span></div>'+
                                        '<div class="col-11 text-muted text-xs">'+fecha_creacion+'</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="card-footer">'+
                                    '<div class="row text-muted text-sm">'+
                                        '<div class="col-7">'+
                                            '<span>Creado por: '+r.data[i].creador+'</span>'+
                                        '</div>'+
                                        '<div class="col-5 d-flex align-items-end justify-content-end">'+                                           
                                            botonera+                                           
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'
                    }
                    
                    //Se agrega o actualiza la fila dependiendo de la accion
                    if(accion == 'crear'){
                        $('#contenido').append(fila)
                    } else {
                        $('#proyecto' + r.data[0].id).replaceWith(fila)
                    }

                    if(habilitadoCrear == 'NO'){
                        texto = 'No hay proyectos de emprendimiento creados, puedes crear el tuyo dando clic en el botón "Crear Proyecto".'
                    }
                } else {
                    let texto = 'Debes estar habilitado por el administrador del sistema para poder crear proyectos de emprendimiento.'

                    if(habilitadoCrear == 'SI'){
                        texto = 'No hay proyectos de emprendimiento creados, puedes crear el tuyo dando clic en el botón "Crear Proyecto".'
                    }

                    let fila = '<div class="row" id="filaNoProyectos">'+
                        '<div class="col-12">'+
                            '<div class="alert alert-info">'+texto+'</div>'
                        '</div>'+
                    '</div>'
                    $('#contenido').append(fila)
                }
            }
                    
            callback()
        })
    }

    function unirseProyecto(idProyecto){
        id = idProyecto

        if(confirm('Realmente desea unirse a este proyecto? Deberá esperar la aprobación del lider')){
            datos = {}
            datos.fk_proyectos = idProyecto

            enviarPeticion('integrantes', 'insert', datos, function(r){
                if(r.ejecuto == true){
                    cargarRegistros({id: idProyecto}, 'actualizar', function(){})
                } else {
                    toastr.error('Se produjo un error al desactivar el usuario, por favor verifique los datos ingresados e intente de nuevo')
                }
            })
        }
    }
    
    function editarProyecto(idProyecto){
        id = idProyecto
        
        enviarPeticion('proyectos', 'select', {id: idProyecto}, function(r){
            if(r.ejecuto == true){
                $.each(r.data[0], function(campo, valor){
                    $('#'+campo).val(valor)
                })
                
                $('#botonCrearProyecto').hide()
                $('#botonActualizarProyecto').show()
                $('#tituloModal').html('Actualizar Proyecto')
                $('#modalProyecto').modal('show')
            } else {
                toastr.error('Se produjo un error al traer los datos del proyecto, por favor verifique los datos ingresados e intente de nuevo')
            }
        })
    }

    function desactivarProyecto(idProyecto){
        id = idProyecto

        if(confirm('Realmente desea desactivar el proyecto?')){
            enviarPeticion('proyectos', 'update', {id: idProyecto, estado: 'Cancelado'}, function(r){
                if(r.ejecuto == true){
                    cargarRegistros({id: id}, 'actualizar', function(){})
                } else {
                    toastr.error('Se produjo un error al desactivar el proyecto, por favor verifique los datos ingresados e intente de nuevo')
                }
            })
        }
    }
</script>
</body>
</html>