<?php require('views/header.php'); ?>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Modelo Canvas de Negocio</h1>
                </div>
                <div class="col-sm-6">
                    <div class="float-sm-right">
                        <div class="btn-group" role="group">
                            <button id="btnProyectos" class="btn btn-info">Volver a Detalles del Proyecto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12 order-1 order-md-2">
                    <div class="card card-primary">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-10">
                                    <form id="formularioCanvas">
                                        <input type="hidden" id="version" name="version" value="1">
                                        <table border="1" cellpadding="10" width="100%" height="600" align="center">
                                            <tr valign="top">
                                                <td rowspan="2" width="20%">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-10">
                                                            <h5>Asociaciones clave</h5>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="float-sm-right">
                                                                <button type="button" class="btnAgregarTipo btn btn-default btn-sm" onclick="abrirModalTipo('asociacionesClave')" title="Agregar" alt="Agregar"><i class="fas fa-plus-square"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="asociacionesClave" class="tipoCanvasGrande"></div>
                                                </td>
                                                <td width="20%">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-10">
                                                            <h5>Actividades clave</h5>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="float-sm-right">
                                                                <button type="button" class="btnAgregarTipo btn btn-default btn-sm" onclick="abrirModalTipo('actividadesClave')" title="Agregar" alt="Agregar"><i class="fas fa-plus-square"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="actividadesClave" class="tipoCanvas"></div>
                                                </td>
                                                <td rowspan="2" colspan="2" width="20%">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-10">
                                                            <h5>Propuestas clave</h5>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="float-sm-right">
                                                                <button type="button" class="btnAgregarTipo btn btn-default btn-sm" onclick="abrirModalTipo('propuestasClave')" title="Agregar" alt="Agregar"><i class="fas fa-plus-square"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="propuestasClave" class="tipoCanvasGrande"></div>
                                                </td>
                                                <td width="20%">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-10">
                                                            <h5>Relaciones con los clientes</h5>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="float-sm-right">
                                                                <button type="button" class="btnAgregarTipo btn btn-default btn-sm" onclick="abrirModalTipo('relacionesClientes')" title="Agregar" alt="Agregar"><i class="fas fa-plus-square"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="relacionesClientes" class="tipoCanvas"></div>
                                                </td>
                                                <td rowspan="2" width="20%">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-10">
                                                            <h5>Segmento de clientes</h5>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="float-sm-right">
                                                                <button type="button" class="btnAgregarTipo btn btn-default btn-sm" onclick="abrirModalTipo('segmentoClientes')" title="Agregar" alt="Agregar"><i class="fas fa-plus-square"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="segmentoClientes" class="tipoCanvasGrande"></div>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td>
                                                    <div class="row mb-2">
                                                        <div class="col-sm-10">
                                                            <h5>Recursos clave</h5>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="float-sm-right">
                                                                <button type="button" class="btnAgregarTipo btn btn-default btn-sm" onclick="abrirModalTipo('recursosClave')" title="Agregar" alt="Agregar"><i class="fas fa-plus-square"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="recursosClave" class="tipoCanvas"></div>
                                                </td>
                                                <td>
                                                    <div class="row mb-2">
                                                        <div class="col-sm-10">
                                                            <h5>Clientes</h5>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="float-sm-right">
                                                                <button type="button" class="btnAgregarTipo btn btn-default btn-sm" onclick="abrirModalTipo('clientes')" title="Agregar" alt="Agregar"><i class="fas fa-plus-square"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="clientes" class="tipoCanvas"></div>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td colspan="3">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-10">
                                                            <h5>Estructura de costos</h5>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="float-sm-right">
                                                                <button type="button" class="btnAgregarTipo btn btn-default btn-sm" onclick="abrirModalTipo('estructuraCostos')" title="Agregar" alt="Agregar"><i class="fas fa-plus-square"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="estructuraCostos" class="tipoCanvas"></div>
                                                </td>
                                                <td colspan="3">
                                                    <div class="row mb-2">
                                                        <div class="col-sm-10">
                                                            <h5>Flujo de ingresos</h5>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="float-sm-right">
                                                                <button type="button" class="btnAgregarTipo btn btn-default btn-sm" onclick="abrirModalTipo('flujoIngresos')" title="Agregar" alt="Agregar"><i class="fas fa-plus-square"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="flujoIngresos" class="tipoCanvas"></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                                <div class="col-2 text-center">
                                    <div id="versiones"></div>
                                    <div>
                                        <button type="submit" class="btn btn-success btn-submit btn-lg" id="botonGuardarCanvas" form="formularioCanvas">Guardar Canvas</button><br /><br />
                                        <button type="submit" class="btn btn-danger btn-sm" id="botonLimpiarCanvas" onclick="limpiarCanvas()">Limpiar Canvas</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id="modalCanvas" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 id="tituloModal" class="modal-title">Crear Post-it</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formularioTipo">
                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <textarea class="form-control" id="descripcion" name="descripcion" rows="4" placeholder="Descripción" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="color">Color:</label>
                        <input type="hidden" id="color" name="color" value="#FFFFFF" />
                        <div id="colores"></div>
                        <div id="colorMuestra">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-submit" id="botonCrearTipo" form="formularioTipo">Crear</button>
            </div>
        </div>
    </div>
</div>

<?php require('views/footer.php');?>
<script type="text/javascript">
    let id = '<?php echo (isset($this->parametros) && sizeof($this->parametros) > 0) ? $this->parametros[0] : 0; ?>'
	let idUser
	let rol
    let tipo
    let versiones = []
    //https://www.w3schools.com/cssref/css_colors.asp
    let colores = ['#FFFFFF', '#F0F0F0', '#F08080', '#FFA500', '#FFFF00', '#FFEBCD', '#FFF8DC', '#7FFF00', '#00FF7F', '#7FFFD4', '#00FFFF', '#EE93EE']

    function init(info){
        if(info.data.length == 0){
            window.location.href = 'main/index'
        } else {
            idUser = info.data.usuario.id
            rol = info.data.usuario.rol
        }

        if(rol != 'Usuario'){
            $('.btnAgregarTipo').addClass('display-none')
            $('#botonGuardarCanvas').addClass('display-none')
            $('#botonLimpiarCanvas').addClass('display-none')
        }
        
        $('#btnProyectos').on('click', function(){
            window.location.href = 'proyectos/detalles/' + id
        })

        cargarRegistros({id: id}, 'crear', function(){})

        $('#formularioTipo').on('submit', function(e){
            e.preventDefault()
            let datos = parsearFormulario($(this))

            $('#descripcion').val('')
            $('#color').val('#FFFFFF')
            $('#modalCanvas').modal('hide')
            agregarTipo(datos.descripcion, datos.color, '')
        })

        $('#formularioCanvas').on('submit', function(e){
            e.preventDefault()
            let datos = parsearFormulario($(this))
            datos.fk_proyectos = id
            let datosCanvas = []

            $('#formularioCanvas .postitActividad').each(function(){
                let texto

                $('#'+$(this).attr('id')).find('span.texto').each(function() {
                    texto = $(this).text()
                    texto = texto.replace(/["']/g, '')
                    texto = texto.replace('\\', '')
                })

                let post = {
                    id: $(this).attr('id'),
                    tipo: $(this).attr('mask-tipo'),
                    color: $(this).attr('mask-color'),
                    texto: texto
                }

                datosCanvas.push(post)
            })
            
            if(datosCanvas.length > 0){
                datos.texto = JSON.stringify(datosCanvas)

                enviarPeticion('canvas', 'insert', datos, function(r){
                    if(r.ejecuto == true){
                        $('#descripcion').val('')
                        $('#color').val('')
                        $('#modalCanvas').modal('hide')
                        window.location.href = 'proyectos/canvas/' + id
                    } else {
                        toastr.error('Se produjo un error al guardar el canvas, por favor verifique los datos e intente de nuevo')
                    }
                })
            } else {
                toastr.info('No hay datos para enviar')
            }
        })

        for(let i = 0; i < colores.length; i++){
            let btnColor = '<div class="colorTipo pointer" style="background-color: '+colores[i]+';" onClick="seleccionarColor(\''+colores[i]+'\')" title="Haga clic para seleccionar este color" alt="Haga clic para seleccionar este color"></div>&nbsp;'
            $('#colores').append(btnColor)
        }

        //No se permiten caracteres como ' o "
        $('#descripcion').on('keypress', function(e){
            var regex = new RegExp('^[a-zA-Z0-9 ._^%$#¡!~@,-<>¿?]+$');
            var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);

            if(!regex.test(key)) {
                e.preventDefault()
                return false
            }
        })
    }

    function cargarRegistros(datos, accion, callback){
        //Informacion de los proyectos
        enviarPeticion('proyectos', 'select', datos, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    //Se verifica que el proyecto este activo
                    if(r.data[0].estado == 'Cancelado' || r.data[0].estado == 'Rechazado'){
                        window.location.href = 'proyectos/index'
                    }

                    //Se verifica que el usuario si sea parte del proyecto, que sea un usuario activo
                    let usuarioActivo = false

                    for(let i = 0; i < r.data[0].listado_integrantes.length; i++){
                        if(idUser == r.data[0].listado_integrantes[i].fk_usuarios && r.data[0].listado_integrantes[i].estado == 'Activo'){
                            usuarioActivo = true
                        }
                    }

                    if(!usuarioActivo && rol != 'Administrador'){
                        window.location.href = 'proyectos/index'
                    }

                    //Se cargan los canvas
                    enviarPeticion('canvas', 'select', {fk_proyectos: id}, function(rc){
                        if(rc.ejecuto == true){
                            if(rc.data.length > 0){
                                let btnVersiones = ''

                                for(let j = 0; j < rc.data.length; j++){
                                    //Se almacena la informacion de todos para su posterior visualizacion
                                    versiones[rc.data[j].version] = rc.data[j]

                                    //Solo se muestra el ultimo canvas
                                    if(j == rc.data.length - 1){
                                        let nuevaVersion = parseInt(rc.data[j].version, 10) + 1
                                        let objeto = JSON.parse(rc.data[j].texto)

                                        for(let k = 0; k < objeto.length; k++){
                                            tipo = objeto[k].tipo
                                            agregarTipo(objeto[k].texto, objeto[k].color, objeto[k].id)
                                        }

                                        $('#version').val(nuevaVersion)
                                    }

                                    btnVersiones += '<button class="btn btn-default btn-sm" onClick="cargarVersion('+rc.data[j].version+')" alt="Ver canvas de esta versión" title="Ver canvas de esta versión">Versión '+rc.data[j].version+'</button>&nbsp;'
                                }

                                $('#versiones').html(btnVersiones)
                            }
                        }
                    })
                }
            }

            callback()
        })
    }

    function seleccionarColor(color){
        $('#color').val(color)
        $('#colorMuestra').css('backgroundColor', color)
    }

    function limpiarCanvas(){
        $('.tipoCanvas').html('')
        $('.tipoCanvasGrande').html('')
    }

    function cargarVersion(idVersion){
        limpiarCanvas()
        let objeto = JSON.parse(versiones[idVersion].texto)

        for(let k = 0; k < objeto.length; k++){
            tipo = objeto[k].tipo
            agregarTipo(objeto[k].texto, objeto[k].color, objeto[k].id)
        }
    }

    function abrirModalTipo(tipoPost){
        tipo = tipoPost
        $('#descripcion').val('')
        $('#color').val('#FFFFFF')
        $('#modalCanvas').modal('show')
    }

    function agregarTipo(texto, color, id){
        let elemento

        if(color == 'undefined' || color == ''){
            color = '#FFFFFF'
        }

        if(id == 'undefined' || id == ''){
            let n = new Date().getTime()
            id = tipo + n
        }

        if(rol == 'Administrador'){
            elemento = '<div id="'+id+'" class="postitActividad" style="background-color: '+color+'" mask-tipo="'+tipo+'" mask-color="'+color+'">'+
                '<span>'+texto+'</span>'+
            '</div>'
        } else {
            elemento = '<div id="'+id+'" class="postitActividad" style="background-color: '+color+'" mask-tipo="'+tipo+'" mask-color="'+color+'">'+
                '<button type="button" class="close" onclick="borrarTipo(\''+id+'\')" title="Quitar" alt="Quitar">'+
                    '<span aria-hidden="true">&times;</span>'+
                '</button>'+
                '<span class="texto">'+texto+'</span>'+
            '</div>'
        }

        $('#'+tipo).append(elemento)
    }

    function borrarTipo(div){
        if(confirm('Está seguro de borrar este elemento?')){
            $('#'+div).remove()
        }
    }
</script>
</script>
</body>
</html>