<?php require('views/header.php'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 id="titulo_proyecto"></h1>
                    </div>
                    <div class="col-sm-6">
                        <div class="float-sm-right">
                            <a href="proyectos/index" class="btn btn-primary">Ver Todos los Proyectos</a>
                            <a href="proyectos/index/1" id="btnMisProyectos" class="btn btn-info">Ir a Mis Proyectos</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-6 order-2 order-md-1">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="info-box">
                                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Creado por</span>
                                        <span class="info-box-number" id="creador_proyecto"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-calendar-alt"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Fecha de creación</span>
                                        <span class="info-box-number" id="fecha_creacion_proyecto"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix hidden-md-up"></div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Integrantes Activos</span>
                                        <span class="info-box-number" id="integrantes_proyecto"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text">Tipo de Proyecto</span>
                                        <span class="info-box-number" id="tipo_proyecto"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix hidden-md-up"></div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon elevation-1" id="span_estado_proyecto"><i class="fas fa-step-forward"></i></span>
                                    <div class="info-box-content">
                                        <div class="float-sm-right">
                                            <button class="btn btn-sm btn-primary display-none" id="cambiarEstado" alt="Cambiar Estado" title="Cambiar Estado"><i class="fas fa-edit"></i></button>
                                        </div>
                                        <span class="info-box-text">Estado</span>
                                        <span class="info-box-number" id="estado_proyecto"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6">
                                <div class="info-box mb-3">
                                    <span class="info-box-icon elevation-1" id="span_fase_proyecto"><i class="fas fa-step-forward"></i></span>
                                    <div class="info-box-content">
                                        <div class="float-sm-right">
                                            <button class="btn btn-sm btn-primary display-none" id="cambiarFase" alt="Cambiar Fase" title="Cambiar Fase"><i class="fas fa-edit"></i></button>
                                        </div>
                                        <span class="info-box-text">Fase</span>
                                        <span class="info-box-number" id="fase_proyecto"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card card-primary card-outline">
                                    <div class="card-header">
                                        <h3 class="card-title"><b>Descripción</b></h3>
                                        <div class="card-tools">
                                        </div>
                                    </div>
                                    <div class="card-body" id="descripcion_proyecto"></div>
                                    <div class="card-footer text-right">
                                        <button class="btn btn-sm btn-primary" id="irCanvas">Ir al Canvas</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="card card-primary card-outline">
                                    <div class="card-header">
                                        <h3 class="card-title"><b>Integrantes</b></h3>
                                        <div class="card-tools">
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn btn-info btn-sm display-none" id="btnAgregarMentorAsesor" onclick="agregarUsuario()">Agregar Asesor/Mentor</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-sm" id="tabla_integrantes">
                                            <thead>
                                                <tr>
                                                    <td width="50%" align="left"><b>Nombre</b></td>
                                                    <td width="20%" align="center"><b>Rol</b></td>
                                                    <td width="20%" align="center"><b>Estado</b></td>
                                                    <td width="10%" align="center"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-6 order-1 order-md-2">
                        <div id="contenedorTareas">
                            <div class="card card-primary card-outline">
                                <div class="card-header">
                                    <h3 class="card-title"><b>Tareas / Actividades</b></h3>
                                    <div class="card-tools">
                                        <button class="btn btn-sm btn-default display-none" id="crearTarea" title="Nueva tarea"><i class="far fa-calendar-plus"></i></button>
                                        <button class="btn btn-sm btn-default" id="historicoTarea" title="Ver histórico"><i class="fas fa-history"></i></button>
                                    </div>
                                </div>
                                <div class="card-body" id="tareasActivas">
                                    <div class="row">
                                        <div class="col-6">
                                            <h6 class="text-center"><b>Por hacer</b></h6>
                                            <hr />
                                            <div class="row">
                                                <div class="col-12" id="tareas_hacer">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <h6 class="text-center"><b>Por revisar</b></h6>
                                            <hr />
                                            <div class="row">
                                                <div class="col-12" id="tareas_revisar">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body display-none" id="tareasTerminadas">
                                    <div class="row">
                                        <div class="col-6">
                                            <h6 class="text-center"><b>Terminadas</b></h6>
                                            <hr />
                                            <div class="row">
                                                <div class="col-12" id="tareas_aceptar">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <h6 class="text-center"><b>Canceladas</b></h6>
                                            <hr />
                                            <div class="row">
                                                <div class="col-12" id="tareas_cancelar">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </span>
        </section>
    </div>

    <div id="modalTarea" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 id="tituloModal" class="modal-title">Crear Tarea</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formularioTarea">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="titulo">Título</label>
                                    <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="descripcion">Descripción</label>
                                    <textarea class="form-control" id="descripcion" name="descripcion" rows="4" placeholder="Descripción de la tarea" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="fk_usuarios">Responsable</label>
                                    <select class="form-control" name="fk_usuarios" id="fk_usuarios" required="required">
                                        <option value="">Seleccione uno</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="fecha_entrega">Fecha de Entrega</label>
                                    <input type="text" class="form-control" id="fecha_entrega" name="fecha_entrega" placeholder="yyyy-mm-dd" required="required" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask>
                                </div>
                                <div class="form-group">
                                    <label for="fk_herramientas">Herramienta</label>
                                    <select class="form-control" name="fk_herramientas" id="fk_herramientas">
                                        <option value="">Seleccione una</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-submit" id="botonCrearTarea" form="formularioTarea">Crear</button>
                    <button type="submit" class="btn btn-secondary btn-submit" id="botonActualizarTarea" form="formularioTarea">Actualizar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalTareaRealizar" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 id="tituloModalTarea" class="modal-title">Ver Tarea</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formularioTareaRealizar">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Título</label>
                                    <p id="titulo_tarea"></p>
                                </div>
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <p id="descripcion_tarea"></p>
                                </div>
                                <div class="form-group">
                                    <label for="comentario_entrega">Detalle</label>
                                    <p id="comentario_entrega_tarea" class="display-none"></p>
                                    <textarea class="form-control" id="comentario_entrega" name="comentario_entrega" rows="2" placeholder="Detalle de la tarea resuelta" required="required"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="enlace">Enlace</label>
                                    <p id="enlace_tarea" class="display-none"></p>
                                    <textarea class="form-control" id="enlace" name="enlace" rows="2" placeholder="Enlace de la tarea resuelta" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Responsable</label>
                                    <p id="responsable_tarea"></p>
                                </div>
                                <div class="form-group">
                                    <label>Fecha de Entrega</label>
                                    <p id="fechaentrega_tarea"></p>
                                </div>
                                <div class="form-group">
                                    <label>Herramienta</label>
                                    <p id="herramienta_tarea"></p>
                                </div>
                                <div class="form-group">
                                    <label for="comentario_calificacion">Detalle de calificación</label>
                                    <p id="comentario_calificacion_tarea" class="display-none"></p>
                                    <textarea class="form-control" id="comentario_calificacion" name="comentario_calificacion" rows="2" placeholder="Detalle de la calificación"></textarea>
                                </div>
                                <div class="form-group" id="divEstadoTarea">
                                    <label for="estadoTarea">Estado</label>
                                    <p id="estado_tarea" class="display-none"></p>
                                    <select class="form-control" name="estado" id="estadoTarea" required="required">
                                        <option value="">Seleccione uno</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-submit" id="botonActualizarTareaRealizar" form="formularioTareaRealizar">Actualizar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalEstado" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">Cambiar Estado del Proyecto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formularioEstado">
                        <div class="form-group">
                            <label for="estado">Nuevo Estado</label>
                            <select class="form-control" name="estado" id="estado"></select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="cambiarEstadoProyecto()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalFase" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title">Cambiar Fase del Proyecto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formularioFase">
                        <div class="form-group">
                            <label for="fk_fasesproyecto">Nueva Fase</label>
                            <select class="form-control" name="fk_fasesproyecto" id="fk_fasesproyecto"></select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="cambiarFaseProyecto()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div id="modalAgregar" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 id="tituloModal" class="modal-title">Agregar Usuario al Proyecto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <p><b>NOTA:</b> Este formulario únicamente es para agregar usuarios Mentores y/o Asesores.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <form id="formularioAsignarUsuario">
                                <div class="form-group">
                                    <label for="usuarioRol">Usuario</label>
                                    <div class="input-group">
                                        <input type="hidden" id="fk_usuarios_rol" name="fk_usuarios_rol" required="required">
                                        <input type="text" class="form-control" id="usuarioRol" name="usuarioRol" placeholder="Buscar por correo o nombre" required="required">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rol">Rol</label>
                                    <div class="input-group">
                                        <select class="form-control" name="rol" id="rol" required="required" disabled="disabled">
                                            <option value="">Seleccione el rol</option>
                                            <option value="Asesor">Asesor</option>
                                            <option value="Mentor">Mentor</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="limpiarCamposRol()">Limpiar</button>
                    <div class="float-sm-right">
                        <button type="submit" class="btn btn-success" form="formularioAsignarUsuario">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require('views/footer.php');?>
<script type="text/javascript">
    let id = '<?php echo (isset($this->parametros) && sizeof($this->parametros) > 0) ? $this->parametros[0] : 0; ?>'
    let idUser
    let rol
    let idTarea
    let rolMentor = false
    let rolAsesor = false

    $(function(){
        enviarPeticion('integrantes', 'select', {fk_proyectos: id, estado: 'Activo'}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){
                        if(r.data[i].rol == 'Lider' || r.data[i].rol == 'Miembro'){
                            $('#fk_usuarios').append('<option value="'+r.data[i].usuario.id+'">'+r.data[i].usuario.nombres+' '+r.data[i].usuario.apellidos+'</option>')
                        }
                    }
                }
            }
        })

        enviarPeticion('proyectos', 'obtenerDatosEnum', {columna: 'estado'}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){
                        $('#estado').append('<option value="'+r.data[i].id+'">'+r.data[i].campo+'</option>')
                    }
                }
            }
        })

        enviarPeticion('tareas', 'obtenerDatosEnum', {columna: 'estado'}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){
                        $('#estadoTarea').append('<option value="'+r.data[i].id+'">'+r.data[i].campo+'</option>')
                    }
                }
            }
        })

        $('#fecha_entrega').daterangepicker({
            singleDatePicker: true,
            minDate: moment(),
            'locale': {
                'format': 'YYYY-MM-DD',
                'separator': ' - ',
                'applyLabel': 'Aplicar',
                'cancelLabel': 'Cancelar',
                'fromLabel': 'De',
                'toLabel': 'a',
                'customRangeLabel': 'Custom',
                'weekLabel': 'W',
                'daysOfWeek': ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                'monthNames': ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                'firstDay': 1
            }
        })

        $('#historicoTarea').on('click', function(){
            if($('#tareasActivas').hasClass('display-none')){
                $('#tareasActivas').removeClass('display-none')
                $('#tareasTerminadas').addClass('display-none')
            } else {
                $('#tareasActivas').addClass('display-none')
                $('#tareasTerminadas').removeClass('display-none')
            }
        })
    })

    function init(info){
        let boton

        if(info.data.length == 0){
            window.location.href = 'main/index'
        } else {
            idUser = info.data.usuario.id
            rol = info.data.usuario.rol
        }

        cargarRegistros({id: id}, 'ver', function(){})
        
        $('#irCanvas').on('click', function(){
            window.location.href = 'proyectos/canvas/' + id
        })
        
        //Creacion de nueva tarea
        $('#crearTarea').on('click', function(){
            $('#titulo').val('')
            $('#descripcion').val('')
            $('#fk_usuarios').val('')
            $('#fecha_entrega').val('')
            $('#fk_herramientas').val('')
            $('#botonCrearTarea').show()
            $('#botonActualizarTarea').hide()
            $('#tituloModal').html('Crear Tarea')
            $('#modalTarea').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })
        
        $('#formularioTarea').on('submit', function(e){
            e.preventDefault()
            let datos = parsearFormulario($(this))
            datos.fk_proyectos = id
            
            if(boton == 'botonCrearTarea'){
                enviarPeticion('tareas', 'insert', datos, function(r){
                    if(r.ejecuto == true){
                        cargarTareas()
                        $('#modalTarea').modal('hide')
                        $('#nombre').val('')
                        $('#descripcion').val('')
                        $('#fk_usuarios').val('')
                        $('#fecha_entrega').val('')
                        $('#fk_herramientas').val('')
                    } else {
                        toastr.error('Se produjo un error al crear la tarea, por favor verifique los datos ingresados e intente de nuevo')
                    }
                })
            } else {
                datos.id = idTarea
                
                enviarPeticion('tareas', 'update', datos, function(r){
                    if(r.ejecuto == true){
                        cargarTareas()
                        $('#modalTarea').modal('hide')
                        $('#nombre').val('')
                        $('#descripcion').val('')
                        $('#fk_usuarios').val('')
                        $('#fecha_entrega').val('')
                        $('#fk_herramientas').val('')
                    } else {
                        toastr.error('Se produjo un error al actualizar la tarea, por favor verifique los datos ingresados e intente de nuevo')
                    }
                })
            }
        })
        
        $('#formularioTareaRealizar').on('submit', function(e){
            e.preventDefault()
            let datos = parsearFormulario($(this))
            datos.id = idTarea
            
            enviarPeticion('tareas', 'update', datos, function(r){
                if(r.ejecuto == true){
                    cargarTareas()
                    $('#modalTareaRealizar').modal('hide')
                    $('#titulo_tarea').html('')
                    $('#descripcion_tarea').html('')
                    $('#enlace_tarea').html('')
                    $('#enlace').val('')
                    $('#responsable_tarea').html('')
                    $('#fechaentrega_tarea').html('')
                    $('#herramienta_tarea').html('')
                    $('#estado_tarea').html('')
                    $('#estado').val('')
                    $('#enlace_tarea').html('').addClass('display-none')
                } else {
                    toastr.error('Se produjo un error al actualizar la tarea, por favor verifique los datos ingresados e intente de nuevo')
                }
            })
        })
        
        //Cambio de estado
        $('#cambiarEstado').on('click', function(){
            $('#modalEstado').modal('show')
        })
        
        //Cambio de fase
        $('#cambiarFase').on('click', function(){
            $('#modalFase').modal('show')
        })

        if(rol == 'Administrador'){
            $('#btnMisProyectos').addClass('display-none')
            $('#btnAgregarMentorAsesor').removeClass('display-none')
        
            enviarPeticion('usuarios', 'select', {rol: ['Usuario'], estado: 'Activo'}, function(r){
                if(r.ejecuto == true){
                    if(r.data.length > 0){
                        let usuariosMentorAsesor = []

                        for (let i = 0; i < r.data.length; i++){
                            usuariosMentorAsesor.push({
                                value: r.data[i].id,
                                label: r.data[i].nombres + ' ' + r.data[i].apellidos + ' - ' + r.data[i].correo,
                                rol: r.data[i].rol
                            })
                        }

                        $('#usuarioRol').autocomplete({
                            minLength: 3,
                            source: usuariosMentorAsesor,
                            focus: function(event, ui) {
                                return false
                            },
                            select: function(event, ui) {
                                event.preventDefault()
                                $('#fk_usuarios_rol').val(ui.item.value)
                                $('#usuarioRol').val(ui.item.label).attr('disabled', true)
                                $('#rol').prop('disabled', false)
                                return false
                            }
                        }).autocomplete('instance')._renderItem = function(ul, item) {
                            return $('<li>').append('<div>' + item.label + '</div>').appendTo(ul)
                        }
                    }
                }
            })

            $('#formularioAsignarUsuario').on('keyup keypress', function(e){
                let keyCode = e.keyCode || e.which
                
                if (keyCode === 13) { 
                    e.preventDefault()
                    return false
                }
            });

            $('#formularioAsignarUsuario').on('submit', function(e){
                e.preventDefault()
                let datos = parsearFormulario($(this))
                datos.estado = 'Activo'
                datos.fk_proyectos = id
                datos.fk_usuarios = datos.fk_usuarios_rol
                delete datos.fk_usuarios_rol
                
                enviarPeticion('integrantes', 'insertAdmin', datos, function(r){
                    if(r.ejecuto == true){
                        toastr.success('Usuario asignado exitosamente.')
                        limpiarCamposRol()
                        $('#modalAgregar').modal('hide')
                        cargarRegistros({id: id}, 'ver', function(){})
                    } else {
                        toastr.error('Se produjo un error al agregar el usuario, por favor verifique los datos ingresados e intente de nuevo.')
                    }
                })
            })
        }
    }

    function cargarRegistros(datos, accion, callback){
        //Informacion del proyecto
        enviarPeticion('proyectos', 'select', datos, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    //Se verifica que el proyecto este activo
                    if(r.data[0].estado == 'Cancelado' || r.data[0].estado == 'Rechazado'){
                        window.location.href = 'proyectos/index'
                    }

                    //Se verifica que el usuario si sea parte del proyecto, que sea un usuario activo
                    let usuarioActivo = false

                    for(let j = 0; j < r.data[0].listado_integrantes.length; j++){
                        if(idUser == r.data[0].listado_integrantes[j].fk_usuarios && r.data[0].listado_integrantes[j].estado == 'Activo'){
                            usuarioActivo = true
                        }
                    }

                    if(!usuarioActivo && rol != 'Administrador'){
                        window.location.href = 'proyectos/index'
                    }

                    let fecha = new Date(r.data[0].fecha_creacion)
                    let anho = new Intl.DateTimeFormat('es-CO', { year: 'numeric' }).format(fecha)
                    let mes = new Intl.DateTimeFormat('es-CO', { month: 'short' }).format(fecha)
                    let dia = new Intl.DateTimeFormat('es-CO', { day: '2-digit' }).format(fecha)
                    let etiqueta = 'primary'

                    if(r.data[0].estado == 'Finalizado'){
                        etiqueta = 'success'
                    }
                    else if(r.data[0].estado == 'Cancelado' || r.data[0].estado == 'Rechazado'){
                        etiqueta = 'danger'
                    }
                    else if(r.data[0].estado == 'Pendiente'){
                        etiqueta = 'warning'
                    }

                    let etiqueta_fase = 'info'

                    if(r.data[0].fk_fasesproyecto != 0)
                        etiqueta_fase = 'primary'

                    $('#titulo_proyecto').html(r.data[0].nombre)
                    $('#creador_proyecto').html(r.data[0].creador)
                    $('#fecha_creacion_proyecto').html(dia + ' ' + mes + ' ' + anho)
                    $('#tipo_proyecto').html(r.data[0].tipo.nombre)
                    $('#estado_proyecto').html(r.data[0].estado)
                    $('#span_estado_proyecto').removeClass('bg-primary').removeClass('bg-success').removeClass('bg-danger').removeClass('bg-warning').addClass('bg-' + etiqueta)
                    $('#fase_proyecto').html(r.data[0].fase.codigo+': '+r.data[0].fase.nombre)
                    $('#span_fase_proyecto').removeClass('bg-primary').removeClass('bg-success').removeClass('bg-danger').removeClass('bg-warning').addClass('bg-' + etiqueta_fase)
                    $('#descripcion_proyecto').html(r.data[0].descripcion)

                    cargarIntegrantes(r.data[0])
                    cargarTareas()
                    cargarFases(r.data[0])
                    cargarHerramientas(r.data[0])
                }
            }

            callback()
        })
    }
    
    function estadoIntegrante(idIntegrante, estado){
        datos = {}
        datos.id = idIntegrante

        if(estado){
            datos.estado = 'Activo'
        } else {
            datos.estado = 'Rechazado'
        }

        enviarPeticion('integrantes', 'update', datos, function(r){
            if(r.ejecuto == true){
                cargarRegistros({id: id}, 'ver', function(){})
            } else {
                toastr.error('Se produjo un error actualizar el integrante, por favor intente de nuevo')
            }
        })
    }

    function cargarIntegrantes(datosProyecto){
        enviarPeticion('integrantes', 'select', {fk_proyectos: id}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    let fila = ''
                    let integrantesActivos = 0
                    let esMentor = false

                    for(let i = 0; i < r.data.length; i++){                        
                        let etiquetaEstado = 'success'
                        let botonera = ''

                        if(r.data[i].estado == 'Activo'){
                            integrantesActivos++

                            etiquetaEstado = 'success'
                            botonera = '<div class="btn-group" role="group">' +
                                '<button class="btn btn-default btn-sm text-red" onClick="estadoIntegrante('+r.data[i].id+', 0)" alt="Rechazar del Proyecto" title="Rechazar del Proyecto"><i class="fas fa-trash"></i></button>' + 
                            '</div>'
                        } else {
                            if(r.data[i].estado == 'Pendiente'){
                                etiquetaEstado = 'warning'
                                botonera =  '<button class="btn btn-default btn-sm text-green" onClick="estadoIntegrante('+r.data[i].id+', 1)" alt="Aceptar en el Proyecto" title="Aceptar en el Proyecto"><i class="fas fa-check"></i></button> '+ 
                                            '<button class="btn btn-default btn-sm text-red" onClick="estadoIntegrante('+r.data[i].id+', 1)" alt="Rechazar del Proyecto" title="Rechazar del Proyecto"><i class="fas fa-trash"></i></button>'
                            } else {
                                etiquetaEstado = 'danger'
                                botonera = '<div class="btn-group" role="group">' +
                                    '<button class="btn btn-success btn-sm" onClick="estadoIntegrante('+r.data[i].id+', 1)" alt="Aceptar en el Proyecto" title="Aceptar en el Proyecto"><i class="fas fa-check"></i></button>' + 
                                '</div>'
                            }
                        }

                        if(r.data[i].rol == 'Lider'){                            
                            botonera = ''
                        }
                        else if(r.data[i].rol == 'Mentor' || r.data[i].rol == 'Asesor'){
                            botonera = ''

                            if(rol == 'Administrador'){
                                botonera = '<div class="btn-group" role="group">' +
                                    '<button class="btn btn-default btn-sm text-red" onClick="estadoIntegrante('+r.data[i].id+', 0)" alt="Rechazar del Proyecto" title="Rechazar del Proyecto"><i class="fas fa-trash"></i></button>' + 
                                '</div>'
                            }
                        }

                        if(idUser != datosProyecto.creado_por){
                            if(rol != 'Administrador'){
                                botonera = ''
                            }

                            if(idUser == r.data[i].fk_usuarios && r.data[i].rol == 'Mentor'){
                                $('#crearTarea').removeClass('display-none')
                                esMentor = true
                                rolMentor = true
                            } else if(idUser == r.data[i].fk_usuarios && r.data[i].rol == 'Asesor'){
                                rolAsesor = true
                            }
                        } else {
                            $('#crearTarea').removeClass('display-none')
                        }

                        if(datosProyecto.estado == 'Finalizado'){
                            botonera = ''
                            $('#crearTarea').addClass('display-none')
                        }
                        
                        fila += '<tr>' +
                            '<td align="left">' + r.data[i].usuario.nombres + ' ' + r.data[i].usuario.apellidos + '</td>' +
                            '<td align="center">'+ r.data[i].rol + '</td>' +
                            '<td align="center"><div class="badge badge-'+etiquetaEstado+'">' + r.data[i].estado + '</div></td>' +
                            '<td>' + botonera + '</td>' +
                        '</tr>'
                    }

                    $('#tabla_integrantes tbody').html(fila)
                    $('#integrantes_proyecto').html(integrantesActivos)

                    if(rol == 'Administrador' || esMentor){
                        $('#cambiarEstado').removeClass('display-none')
                        $('#cambiarFase').removeClass('display-none')
                    }
                }
            }
        })
    }

    function cargarTareas(){
        enviarPeticion('tareas', 'select', {fk_proyectos: id}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    let fila_hacer = ''
                    let fila_revisar = ''
                    let fila_aceptar = ''
                    let fila_cancelar = ''

                    for(let i = 0; i < r.data.length; i++){
                        if(r.data[i].estado == 'Pendiente'){
                             fila_hacer += '<div class="small-box bg-warning">' +
                                    '<div class="inner">' +
                                        '<h4>' + r.data[i].titulo + '</h4>' +
                                        '<p><b>Fecha de entrega:</b> ' + r.data[i].fecha_entrega + '</p>' +
                                    '</div>'

                            if(rolMentor){
                               fila_hacer += '<a href="#" class="small-box-footer boton_tarea" onclick="editarTarea(' + r.data[i].id + ')">Editar Tarea <i class="fas fa-arrow-circle-right"></i></a>'
                            } else if(rol == 'Usuario') {
                                 fila_hacer += '<a href="#" class="small-box-footer boton_tarea" onclick="abrirModalVerTarea(\'actualizar\', ' + r.data[i].id + ')">Entregar Tarea <i class="fas fa-arrow-circle-right"></i></a>'
                            }

                            fila_hacer += '</div>'
                        } else if (r.data[i].estado == 'Entregada'){
                            fila_revisar += '<div class="small-box bg-info">' +
                                    '<div class="inner">' +
                                        '<h4>' + r.data[i].titulo + '</h4>' +
                                        '<p><b>Fecha de entrega:</b> ' + r.data[i].fecha_entrega + '</p>' +
                                        '<p><b>Fecha de envío:</b> ' + r.data[i].fecha_modificacion+ '</p>' +
                                    '</div>'

                            if(rolMentor){
                               fila_revisar += '<a href="#" class="small-box-footer boton_tarea" onclick="abrirModalVerTarea(\'revisar\', ' + r.data[i].id + ')">Revisar Tarea <i class="fas fa-arrow-circle-right"></i></a>'
                            } else if(rolAsesor){
                               fila_revisar += '<a href="#" class="small-box-footer boton_tarea" onclick="abrirModalVerTarea(\'ver\', ' + r.data[i].id + ')">Ver Tarea <i class="fas fa-arrow-circle-right"></i></a>'
                            }
                                    
                            fila_revisar += '</div>'
                        } else if (r.data[i].estado == 'Aprobada'){
                            fila_aceptar += '<div class="small-box bg-success">' +
                                    '<div class="inner">' +
                                        '<h4>' + r.data[i].titulo + '</h4>' +
                                        '<p><b>Fecha de entrega:</b> ' + r.data[i].fecha_entrega + '</p>' +
                                        '<p><b>Fecha de aceptación:</b> ' + r.data[i].fecha_modificacion+ '</p>' +
                                    '</div>'

                            if(rol != 'Administrador'){
                               fila_aceptar += '<a href="#" class="small-box-footer boton_tarea" onclick="abrirModalVerTarea(\'ver\', ' + r.data[i].id + ')">Ver Tarea <i class="fas fa-arrow-circle-right"></i></a>'
                            }

                            fila_aceptar += '</div>'
                        } else if (r.data[i].estado == 'Cancelada'){
                            fila_cancelar += '<div class="small-box bg-danger">' +
                                    '<div class="inner">' +
                                        '<h4>' + r.data[i].titulo + '</h4>' +
                                        '<p><b>Fecha de entrega:</b> ' + r.data[i].fecha_entrega + '</p>' +
                                        '<p><b>Fecha de cancelación:</b> ' + r.data[i].fecha_modificacion+ '</p>' +
                                    '</div>'

                            if(rol != 'Administrador'){
                               fila_cancelar += '<a href="#" class="small-box-footer boton_tarea" onclick="abrirModalVerTarea(\'ver\', ' + r.data[i].id + ')">Ver Tarea <i class="fas fa-arrow-circle-right"></i></a>'
                            }

                            fila_cancelar += '</div>'
                        }
                    }

                    $('#tareas_hacer').html(fila_hacer)
                    $('#tareas_revisar').html(fila_revisar)
                    $('#tareas_aceptar').html(fila_aceptar)
                    $('#tareas_cancelar').html(fila_cancelar)

                    $('a.boton_tarea').click(function(event){
                        event.preventDefault()
                    })
                }
            }
        })
    }

    function cargarFases(datosProyecto){
        $('#fk_fasesproyecto option').remove()
        enviarPeticion('fasesproyecto', 'select', {fk_tiposproyecto: datosProyecto.fk_tiposproyecto, estado: 'Activo'}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){
                        $('#fk_fasesproyecto').append('<option value="'+r.data[i].id+'">'+r.data[i].codigo+': '+r.data[i].nombre+'</option>')
                    }
                }
            }
        })
    }

    function cargarHerramientas(datosProyecto){
        enviarPeticion('herramientas', 'selectFase', {fk_tiposproyecto: datosProyecto.fk_tiposproyecto}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){
                        let nombreFase = (r.data[i].fk_fasesproyecto == 0) ? r.data[i].fase.codigo : r.data[i].fase.codigo+': '+r.data[i].fase.nombre
                        $('#fk_herramientas').append('<option value="'+r.data[i].id+'">'+r.data[i].nombre+' ('+nombreFase+')</option>')
                    }
                }
            }
        })
    }
    
    function editarTarea(idTareaEditar){
        idTarea = idTareaEditar
        
        enviarPeticion('tareas', 'select', {id: idTarea}, function(r){
            if(r.ejecuto == true){
                $.each(r.data[0], function(campo, valor){
                    $('#'+campo).val(valor)
                })
                
                $('#botonCrearTarea').hide()
                $('#botonActualizarTarea').show()
                $('#tituloModal').html('Actualizar Tarea')
                $('#modalTarea').modal('show')
            } else {
                toastr.error('Se produjo un error al traer los datos de la tarea, por favor verifique los datos ingresados e intente de nuevo')
            }
        })
    }

    function abrirModalVerTarea(tipo, idTareaEditar){
        idTarea = idTareaEditar
        
        $('#titulo_tarea').html('')
        $('#descripcion_tarea').html('')
        $('#enlace_tarea').html('')
        $('#enlace').val('')
        $('#responsable_tarea').html('')
        $('#fechaentrega_tarea').html('')
        $('#herramienta_tarea').html('')
        $('#estado_tarea').html('')
        $('#estado').val('')
        $('#enlace_tarea').html('').addClass('display-none')
        
        enviarPeticion('tareas', 'selectJoin', {id: idTarea}, function(r){
            if(r.ejecuto == true){
                let herramienta = r.data[0].herramienta.nombre
                let enlace = '<a href="' + r.data[0].enlace + '" target="_blank" class="btn btn-sm btn-info">Clic para ir</a>'
                let comentario_calificacion = '-'

                if(r.data[0].herramienta.enlace != null)
                    if(r.data[0].herramienta.enlace != '')
                        herramienta = ' <a href="' + r.data[0].herramienta.enlace + '" target="_blank" class="btn btn-sm btn-info">' + r.data[0].herramienta.nombre + '</a>'

                if(r.data[0].comentario_calificacion != null)
                    if(r.data[0].comentario_calificacion != '')
                        comentario_calificacion = r.data[0].comentario_calificacion

                $('#tituloModalTarea').html(tipo.charAt(0).toUpperCase() + tipo.slice(1) + " tarea")
                $('#titulo_tarea').html(r.data[0].titulo)
                $('#descripcion_tarea').html(r.data[0].descripcion)
                $('#responsable_tarea').html(r.data[0].usuario.nombres + ' ' + r.data[0].usuario.apellidos)
                $('#fechaentrega_tarea').html(r.data[0].fecha_entrega)
                $('#herramienta_tarea').html(herramienta)
                $('#estado_tarea').html(r.data[0].estado)
                $('#estadoTarea').val(r.data[0].estado)

                if(r.data[0].comentario_entrega != null)
                    if(r.data[0].comentario_entrega != '')
                        $('#comentario_entrega').val(r.data[0].comentario_entrega)

                if(r.data[0].enlace != null)
                    if(r.data[0].enlace != '')
                        $('#enlace').val(r.data[0].enlace)

                if(tipo == 'actualizar'){
                    //Enviar tarea para calificar
                    $('#comentario_calificacion').hide().removeAttr('required')
                    $('#comentario_calificacion_tarea').html(comentario_calificacion).removeClass('display-none')
                    $('#divEstadoTarea').hide()
                    $('#estadoTarea').val('Entregada').removeAttr('required')
                } else if(tipo == 'ver'){
                    //Ver tarea
                    $('#comentario_entrega').hide().removeAttr('required')
                    $('#comentario_entrega_tarea').html(r.data[0].comentario_entrega).removeClass('display-none')
                    $('#comentario_calificacion').hide().removeAttr('required')
                    $('#comentario_calificacion_tarea').html(comentario_calificacion).removeClass('display-none')
                    $('#enlace').hide().removeAttr('required')
                    $('#enlace_tarea').html(enlace).removeClass('display-none')
                    $('#estadoTarea').hide().removeAttr('required')
                    $('#estado_tarea').removeClass('display-none')
                    $('#botonActualizarTareaRealizar').addClass('display-none')
                } else {
                    //Calificar tarea
                    $('#comentario_entrega').hide().removeAttr('required')
                    $('#comentario_entrega_tarea').html(r.data[0].comentario_entrega).removeClass('display-none')
                    $('#comentario_calificacion').attr('required', 'required')
                    $('#enlace').hide().removeAttr('required')
                    $('#enlace_tarea').html(enlace).removeClass('display-none')
                }
                
                $('#modalTareaRealizar').modal('show')
            } else {
                toastr.error('Se produjo un error al traer los datos de la tarea, por favor verifique los datos ingresados e intente de nuevo')
            }
        })
    }

    function cambiarEstadoProyecto(){
        let datos = parsearFormulario($('#formularioEstado'))
        datos.id = id
        
        enviarPeticion('proyectos', 'update', datos, function(r){
            if(r.ejecuto == true){
                $('#modalEstado').modal('hide')
                cargarRegistros({id: id}, 'ver', function(){})
            } else {
                toastr.error('Se produjo un error al actualizar el proyecto, por favor verifique los datos ingresados e intente de nuevo')
            }
        })
    }

    function cambiarFaseProyecto(){
        let datos = parsearFormulario($('#formularioFase'))
        datos.id = id
        
        enviarPeticion('proyectos', 'updateFase', datos, function(r){
            if(r.ejecuto == true){
                $('#modalFase').modal('hide')
                cargarRegistros({id: id}, 'ver', function(){})
            } else {
                toastr.error('Se produjo un error al actualizar el proyecto, por favor verifique los datos ingresados e intente de nuevo')
            }
        })
    }

    function agregarUsuario(){
        limpiarCamposRol()
        $('#modalAgregar').modal('show')
    }

    function limpiarCamposRol(){
        $('#fk_usuarios').val('')
        $('#usuarioRol').val('').attr('disabled', false)
        $('#rol').val('').prop('disabled', true)
    }
</script>
</body>
</html>