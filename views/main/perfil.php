<?php require('views/header.php'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Mi Perfil</h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
				<div class="card">
					<form id="formulario">
						<div class="card-body">
							<div class="row">
								<div class="col-12">
									<div class="form-group">
										<label for="correo">Correo electrónico</label>
                       					<input type="email" class="form-control" name="correo" id="correo" placeholder="Correo electrónico" disabled="disabled">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<label for="nombres">Nombre(s)</label>
                        				<input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombre(s)" required="required">
									</div>
									<div class="form-group">
										<label for="numero_contacto">Número de contacto</label>
										<input type="number" class="form-control" name="numero_contacto" id="numero_contacto" placeholder="Número de contacto" required="required" min="0">
									</div>
									<div class="form-group">
										<label class="form-check-label" for="habilitado_proyectos">Puede crear proyectos de emprendimiento</label>
                        				<input type="text" class="form-control col-2" name="habilitado_proyectos" id="habilitado_proyectos" placeholder="SI / NO" disabled="disabled">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<label for="apellidos">Apellido(s)</label>
                        				<input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellido(s)" required="required">
									</div>
									<div class="form-group">
										<label for="rol">Perfil</label>
										<input type="text" class="form-control" name="rol" id="rol" placeholder="Perfil" disabled="disabled">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<div class="alert alert-info">
										<b>NOTA:</b> Solo llena estos dos campos si deseas cambiar la contraseña.
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<div class="form-group">
										<label for="password1">Contraseña</label>
                        				<input type="password" class="form-control" name="password" id="password1" placeholder="Contraseña">
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<label for="password2">Repite la contraseña</label>
										<input type="password" class="form-control" placeholder="Repite la contraseña" id="password2">
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button type="submit" class="btn btn-primary">Actualizar Perfil</button>
						</div>
					</form>
				</div>
            </div>
        </section>
    </div>

<?php require('views/footer.php');?>
<script type="text/javascript">
    let id

    $(function(){
    	//Se crea evento para recibir registro
        $('#formulario').on('submit', function(e){
            e.preventDefault()                
            if(($.trim($('#password1').val()) != '' || $.trim($('#password2').val()) != '') && $.trim($('#password1').val()) != $.trim($('#password2').val())){
                toastr.error('Las contraseñas deben ser iguales')
            } else {
                let datos = parsearFormulario($(this))
                datos.id = id

                if($.trim($('#password1').val()) == '' || $.trim($('#password2').val()) == ''){
                	delete datos.password
                }

                enviarPeticion('usuarios', 'updateAdmin', datos, function(r){
                	if(r.ejecuto == true){
                		cargarRegistros({id: id}, 'crear', function(){
                			toastr.success('Perfil actualizado exitosamente!')
                		})
                    } else {
                        toastr.error('Se produjo un error al actualizar los datos, por favor verifique los datos ingresados e intente de nuevo')
                    }
                })
            }                
        })
    })

    function init(info){
        if(info.data.length == 0){
            window.location.href = 'main/index'
        } else {
            id = info.data.usuario.id
        }

        cargarRegistros({id: id}, 'crear', function(){})
    }
    
    function cargarRegistros(datos, accion, callback){
        //Informacion del usuario
        enviarPeticion('usuarios', 'select', datos, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
		            $.each(r.data[0], function(campo, valor){
		                $('#'+campo).val(valor)
		            })

		            $('#password1').val('')
		            $('#password2').val('')
                }
            }

            callback()
        })
    }
</script>
</body>
</html>