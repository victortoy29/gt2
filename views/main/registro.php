<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Plataforma de Emprendimiento - Unicatolica</title>
    <base href="/gt2/">
    
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="favicon.png">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- CSS de la plataforma -->
    <link rel="stylesheet" href="dist/css/generales.css">
</head>
<body class="hold-transition login-page" style="background-image: url('dist/img/registro.jpg');background-size: cover;background-position: center;background-repeat: no-repeat;">
    <div class="register-box">        
        <div class="card">
            <div class="card-body register-card-body">
                <p class="login-box-msg">Registrese en la plataforma de emprendimiento de Unicatólica!</p>
                <form id="formulario">
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" name="correo" id="correo" placeholder="Correo electrónico" required="required">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="nombres" placeholder="Nombre(s)" required="required">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="apellidos" placeholder="Apellido(s)" required="required">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="number" class="form-control" name="numero_contacto" placeholder="Número de contacto" required="required" min="0">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-phone"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="password" id="password1" placeholder="Contraseña" required="required">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Repite la contraseña" id="password2" required="required">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="interes" name="interes" required="required">
                            <option value="">Motivo de ingreso a la plataforma</option>
                            <option value="Aprender">Quiero Aprender</option>
                            <option value="Emprender">Quiero Emprender</option>
                            <option value="Mentor">Quiero ser Mentor</option>
                            <option value="Asesor">Quiero ser Asesor</option>
                        </select>
                    </div>
                    <div class="form-group display-none" id="div_descripcion">
                        <textarea class="form-control" id="descripcion" name="descripcion" placeholder="Por favor describa brevemente de que trata su emprendimiento" rows="3"></textarea>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="icheck-primary">
                                <input type="checkbox" id="agreeTerms" required="required">
                                <label for="agreeTerms">
                                    Estoy de acuerdo con los <a href="https://www.unicatolica.edu.co/files/directriz-tratamiento-datos-personales-unicatolica.pdf" target="_blank">términos y condiciones</a>
                                    <!--a id="botonTerminos" href="#" data-toggle="modal" data-href="main/terminoscondiciones/">términos y condiciones</a-->
                                </label>
                            </div>
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn btn-primary btn-block">Registrarse</button>
                        </div>
                    </div>
                </form>
                <br>
                <div class="row">
                    <div class="col-10">
                        <a href="main/ingreso" class="text-center">Ya estoy registrado, quiero ingresar</a>        
                    </div>
                    <div class="col">
                        <a href="main/index"><i class="fas fa-home"></i></a>
                    </div>
                </div>                
            </div>
        </div>
    </div>
    
    <!-- Modal terminos y condiciones -->
    <div class="modal fade" id="modalTerminos" tabindex="-1" role="dialog" aria-labelledby="modalTerminos" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title">Términos y condiciones</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SweetAlert2 -->
    <script src="plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="plugins/toastr/toastr.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <script src="dist/js/funciones.js"></script>
    <script type="text/javascript"> 
        $(function(){
            $('#correo').focus()

            //Modal de terminos y condiciones
            $('#botonTerminos').on('click',function(e){
                e.preventDefault()
                let dataURL = $(this).attr('data-href')
                
                $('.modal-body').load(dataURL, function(){
                    $('#modalTerminos').modal({show:true})
                })
            })
            
            //Seleccion del motivo de ingreso
            $('#interes').on('change', function(e){
                if($(this).val() == 'Emprender'){
                    $('#descripcion_').val('').attr('required', 'required')
                    $('#div_descripcion').show()
                } else {
                    $('#descripcion').val('').removeAttr('required')
                    $('#div_descripcion').hide()
                }
            })
            
            //Se crea evento para recibir registro
            $('#formulario').on('submit', function(e){
                e.preventDefault()                
                if($('#password1').val() != $('#password2').val()){
                    toastr.error('Las contraseñas deben ser iguales')
                }else{                    
                    let datos = parsearFormulario($(this))

                    enviarPeticion('usuarios', 'insert', datos, function(r){
                        window.location.href = 'main/index'
                    })
                }                
            })
        })
    </script>
</body>
</html>