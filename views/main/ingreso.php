<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Plataforma de Emprendimiento - Unicatolica</title>
    <base href="/gt2/">
    
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="favicon.png">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- CSS de la plataforma -->
    <link rel="stylesheet" href="dist/css/generales.css">
</head>
<body class="hold-transition login-page" style="background-image: url('dist/img/login.jpg');background-size: cover;background-position: center;background-repeat: no-repeat;">
    <div class="login-box">        
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Por favor inicie sesión</p>
                <form id="formulario">
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" name="correo" id="correo" placeholder="Correo electrónico" required="required">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="password" placeholder="Contraseña" required="required">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                        </div>
                    </div>
                </form>
                <br>
                <div class="row">
                    <div class="col-10">
                        <a href="main/registro/" class="text-center">Soy nuevo, quiero registrarme</a>
                    </div>
                    <div class="col">
                        <a href="main/index/"><i class="fas fa-home"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SweetAlert2 -->
    <script src="plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="plugins/toastr/toastr.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <script src="dist/js/funciones.js"></script>
    <script type="text/javascript"> 
        $(function(){
            $('#correo').focus()

            //Se crea evento para recibir ingreso
            $('#formulario').on('submit', function(e){
                e.preventDefault()
                let datos = parsearFormulario($(this))
                enviarPeticion('usuarios', 'ingreso', datos, function(r){
                    window.location.href = 'main/index'
                })
            })
        })
    </script>
</body>
</html>