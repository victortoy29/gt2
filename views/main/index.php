<?php require('views/header.php'); ?>

<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 text-center">
                <h1 class="font-weight-light text-white"><b>Plataforma de emprendimiento de base tecnológica, desarrollo y transferencia de tecnología para UNICATOLICA</b></h1>
                <p class="lead text-white">Eres parte de la familia UNICATÓLICA<br />"Aprende, crea y emprende en familia"</p>
            </div>
        </div>
    </div>
</header>

<section class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="info-box">
                    <span class="info-box-icon bg-verde"><i class="fas fa-users"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text indexp">Usuarios registrados</span>
                        <span class="info-box-number" id="cantidad_usuarios">0</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-box">
                    <span class="info-box-icon bg-verde"><i class="fas fa-project-diagram"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text indexp">Proyectos en curso</span>
                        <span class="info-box-number" id="cantidad_proyectos">0</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-box">
                    <span class="info-box-icon bg-verde"><i class="fas fa-tools"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text indexp">Herramientas disponibles<span>
                        <span class="info-box-number" id="cantidad_herramientas">0</span>
                    </div>
                </div>
            </div>
        </div>

        <hr class="divisor">

        <div class="row">
            <div class="col-md-5">                
                <img src="dist/img/centro.jpg" class="img-fluid" alt="Unidad de Emprendimiento">
            </div>
            <div class="col-md-6">
                <h4>
                    Unidad de emprendimiento de base tecnológica, desarrollo y transferencia de tecnología
                </h4>
                <p class="indexp text-justify">
                    La unidad de emprendimiento de base tecnológica, desarrollo y transferencia de tecnología tiene como propósito fundamental fomentar el desarrollo de capacidades en emprendimiento más allá del currículo, disponiendo de infraestructura, contenidos e iniciativas que permitan a los estudiantes orientar su vocación profesional en esta vía, bien para creación de empresas, como para la comercialización de tecnologías y gerencia de proyectos.
                </p>                
            </div>            
        </div>

        <hr class="divisor">

        <div class="row">
            <div class="col-md-7 table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <th width="30%" style="background-color: #DEDEDE;"><span class="indexp">PORTAFOLIO</span></th>
                        <th style="background-color: #DEDEDE;"><span class="indexp">PRODUCTOS Y SERVICIOS</span></th>
                    </tr>
                    <tr>
                        <td><span class="indexp">EMPRENDIMIENTO DE BASE TECNOLÓGICA (EBT)</span></td>
                        <td>
                            <ul>
                                <li><span class="indexp">Mentoría en Emprendimiento de Base Tecnológica (EBT)</span></li>
                                <li><span class="indexp">Formación en emprendimiento para: Desarrollo de competencias, Intraemprendimiento, Creación de empresa</span></li>
                                <li><span class="indexp">Gestión de proyectos de base tecnológica</span></li>
                                <li><span class="indexp">Orientación en Modelo de negocio</span></li>
                                <li><span class="indexp">Orientación para la Formalización empresarial</span></li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td><span class="indexp">DESARROLLO Y TRANSFERENCIA TECNOLÓGICA</span></td>
                        <td>
                            <ul>
                                <li><span class="indexp">Desarrollo tecnológico</span></li>
                                <li><span class="indexp">Innovación</span></li>
                                <li><span class="indexp">Transferencia de tecnologías desarrolladas y buenas prácticas a empresas</span></li>
                                <li><span class="indexp">Fortalecimiento de redes de conocimiento</span></li>
                                <li><span class="indexp">Capacitación para actualización empresarial, nuevos modelos de negocio, nuevas tecnologías</span></li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-5">
                <img src="dist/img/modelo.jpg" class="img-fluid" alt="Modelo">
            </div>
        </div>

        <hr class="divisor">

        <div class="row">            
            <div class="col text-center mb-5">
                <h4>
                    Roles
                </h4>                
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center">
                <h5 class="bg-verde">Mentores</h5>
                <p class="indexp text-justify">
                    Líder que orienta la ruta de emprendimiento desde las ideas hasta el mercado.
                </p>
            </div>
            <div class="col-md-4 text-center">
                <h5 class="bg-verde">Asesores</h5>
                <p class="indexp text-justify">
                    Expertos temáticos que apoyan la mentoría en emprendimiento y desarrollo tecnológico desde un área de conocimiento específica.
                </p>
            </div>
            <div class="col-md-4 text-center">
                <h5 class="bg-verde">Gestores de desarrollo y transferencia de tecnología</h5>
                <p class="indexp text-justify">
                   Experto en gestión y desarrollo de tecnologías que acompaña su desarrollo logrando el avance en los niveles de madurez requeridos hasta su transferencia al mercado.
                </p>
            </div>
        </div>

        <hr class="divisor">

        <div class="row">
            <div class="col-md-12">
                <img src="dist/img/home3.png" class="img-fluid" alt="Invitación">
            </div>
        </div>
    </div>
</section>

<?php require('views/footer.php');?>
<script type="text/javascript">
    function init(info){
        enviarPeticion('usuarios', 'cantidad', {1:1}, function(r){
            $('#cantidad_usuarios').text(r.data[0].cantidad)
        })
        enviarPeticion('proyectos', 'cantidad', {1:1}, function(r){
            $('#cantidad_proyectos').text(r.data[0].cantidad)
        })
        enviarPeticion('herramientas', 'cantidad', {1:1}, function(r){
            $('#cantidad_herramientas').text(r.data[0].cantidad)
        })
    }
</script>
</body>
</html>