<?php require('views/header.php'); ?>
<?php
$error = 404;

if(sizeof($this->parametros) > 0){
	if($this->parametros[0] == 500){
		$error = 500;
	}
}
?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
        	<?php if($error == 500): ?>
            	<div class="error-page">
					<h2 class="headline text-danger"> 500</h2>
					<div class="error-content">
						<h3><i class="fas fa-exclamation-triangle text-danger"></i> Oops! Algo salió mal.</h3>
						<p>Estamos trabajando en este inconveniente. Mientras tanto, puedes <a href="main/index">regresar a la página de inicio</a>.</p>
					</div>
				</div>
			<?php else: ?>
            	<div class="error-page">
					<h2 class="headline text-warning"> 404</h2>
					<div class="error-content">
						<h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Página no encontrada.</h3>
						<p>No pudimos encontrar la página que estás buscando. Mientras tanto, puedes <a href="main/index">regresar a la página de inicio</a>.</p>
					</div>
				</div>
			<?php endif; ?>
            </div>
        </section>
    </div>

<?php require('views/footer.php'); ?>
</body>
</html>