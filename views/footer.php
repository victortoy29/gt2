<footer class="main-footer">
            <div class="row">
                <div class="col-md-8">
                    <strong>Fundación Universitaria Católica - Lumen Gentium</strong><br><a href="https://www.unicatolica.edu.co/files/directriz-tratamiento-datos-personales-unicatolica.pdf" target="_blank">Tratamiento de datos personales</a>
                </div>
                <div class="col-md-4 text-right">                    
                    <div>Desarrollado por Semillero <a href="https://www.unicatolica.edu.co/grupo-investigacion-khimera/#semilleros" target="_blank">AYNI</a></div>
                </div>
            </div>
        </footer>
    </div>

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="plugins/moment/moment.min.js"></script>
<!-- Daterange-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="plugins/datatables/accent-neutralise.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="plugins/toastr/toastr.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="dist/js/funciones.js"></script>
<script type="text/javascript">
    $(function(){        
        //Menu de usuario
        enviarPeticion('helpers', 'getSession', {}, function(r){
            if(r.data.length == 0){
                $('#navbarCollapse').append(
                    '<ul class="navbar-nav ml-auto">'+
                        '<li class="nav-item">'+
                           '<a href="main/ingreso" class="nav-link">Ingresar</a>'+
                       '</li>'+
                       '<li class="nav-item">'+
                           '<a href="main/registro" class="nav-link">Registrarse</a>'+
                       '</li>'+
                    '</ul>')                
            }else{
                let menuAdmin = ''
                if(typeof(r.data.usuario) == 'undefined'){
                    $('#navbarCollapse').append(
                        '<ul class="navbar-nav ml-auto">'+
                            '<li class="nav-item">'+
                               '<a href="main/ingreso" class="nav-link">Ingresar</a>'+
                           '</li>'+
                           '<li class="nav-item">'+
                               '<a href="main/registro" class="nav-link">Registrarse</a>'+
                           '</li>'+
                        '</ul>')   
                } else {
                    if(r.data.usuario.rol == 'Administrador'){
                        menuAdmin = '<li class="nav-item dropdown">'+
                                        '<a id="menuConfiguracion" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Configuración</a>'+
                                        '<ul aria-labelledby="menuConfiguracion" class="dropdown-menu border-0 shadow">'+
                                            '<li>'+
                                                '<a href="usuarios/listado/" class="dropdown-item">Usuarios</a>'+
                                            '</li>'+
                                            '<li>'+
                                                '<a href="herramientas/listado/" class="dropdown-item">Herramientas</a>'+
                                            '</li>'+
                                        '</ul>'+
                                    '</li>'+
                                    '<li class="nav-item dropdown">'+
                                        '<a id="menuReportes" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Reportes</a>'+
                                        '<ul aria-labelledby="menuReportes" class="dropdown-menu border-0 shadow">'+
                                            '<li>'+
                                                '<a href="reportes/index/" class="dropdown-item">Proyectos por Tipo y Fase</a>'+
                                            '</li>'+
                                            '<li>'+
                                                '<a href="reportes/proyectostipo/" class="dropdown-item">Proyectos por Tipo</a>'+
                                            '</li>'+
                                        '</ul>'+
                                    '</li>'
                    }

                    $('#navbarCollapse').append('<ul class="navbar-nav">'+
                                                    menuAdmin+
                                                    '<li class="nav-item">'+
                                                        '<a href="proyectos/index/" class="nav-link">Proyectos</a>'+
                                                    '</li>'+
                                                '</ul>'+
                                                '<ul class="navbar-nav ml-auto">'+
                                                    '<li class="nav-item dropdown">'+
                                                       '<a id="menuUser" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">'+r.data.usuario.nombres+' '+r.data.usuario.apellidos+'</a>'+
                                                        '<ul aria-labelledby="menuUser" class="dropdown-menu border-0 shadow">'+                                            
                                                            '<li>'+
                                                                '<a href="main/perfil/" class="dropdown-item">Perfil</a>'+
                                                            '</li>'+
                                                            '<li>'+
                                                                '<a href="#" class="dropdown-item" id="salir">Salir</a>'+
                                                            '</li>'+
                                                        '</ul>'+
                                                    '</li>'+
                                                '</ul>')
                    $('#salir').on('click', function(){
                        enviarPeticion('helpers', 'destroySession', {}, function(r){
                            window.location.href = 'main/index'
                        })
                    })
                }
            }
            init(r)
        })
    })  
</script>