<?php require('views/header.php'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Cantidad de Proyectos por Tipo y Fase</h1>
                    </div>
                    <div class="col-sm-6">
                        <div class="float-sm-right">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row" id="fila_reportes">
                </div>
            </div>
        </section>
    </div>

<?php require('views/footer.php');?>
<script type="text/javascript">
	let datosGraficos = []
	let coloresGrafico = ['#F56954', '#00A65A', '#f39C12', '#00C0EF', '#3C8DBC', '#D2D6DE','#F0F0F0', '#F08080', '#FFA500', '#FFFF00', '#7FFF00', '#00FF7F', '#7FFFD4', '#00FFFF', '#EE93EE', '#FFEBCD', '#FFF8DC', '#DEDEDE', '#333333']

    function init(info){
        if(info.data.length == 0){
            window.location.href = 'main/index'
        } else {
            if(info.data.usuario.rol != 'Administrador'){
                window.location.href = 'main/index'
            }
        }
        
        cargarRegistros({}, 'crear', function(){})
    }
    
    function cargarRegistros(datos, accion, callback){
        //Informacion de los proyectos
        enviarPeticion('proyectos', 'selectReporteProyectosTipoFase', datos, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    let fila = ''
                    let tipos = []
                    
                    for(let i = 0; i < r.data.length; i++){
                    	let data = r.data[i]

                    	if(typeof(tipos[data.fk_tiposproyecto]) == 'undefined'){
                    		tipos[data.fk_tiposproyecto] = []
                    	}

                    	if(typeof(tipos[data.fk_tiposproyecto]['labels']) == 'undefined'){
                    		tipos[data.fk_tiposproyecto]['labels'] = []
                    	}

                    	if(typeof(tipos[data.fk_tiposproyecto]['valores']) == 'undefined'){
                    		tipos[data.fk_tiposproyecto]['valores'] = []
                    	}

                    	tipos[data.fk_tiposproyecto]['id'] = data.fk_tiposproyecto
                    	tipos[data.fk_tiposproyecto]['titulo'] = data.tipo
                    	tipos[data.fk_tiposproyecto]['labels'].push(data.fase)
                    	tipos[data.fk_tiposproyecto]['valores'].push(data.cantidad)
                    }

                    for(let i in tipos){
                    	let data = tipos[i]

                    	fila += '<div class="col-6">' +
			                    	'<div class="card card-primary">' +
										'<div class="card-header">' +
											'<h3 class="card-title">' + data['titulo'] + '</h3>' +
										'</div>' +
										'<div class="card-body">' +
											'<canvas id="grafico'+ data['id'] +'" style="min-height: 300px; height: 300px; max-height: 300px; max-width: 100%;"></canvas>' +
										'</div>' +
									'</div>' +
			                    '</div>'

			            datosGraficos.push(data)
                    }
                    
                    $('#fila_reportes').append(fila)
                }
            }
            callback()
        })
    }

	function cargarCanvas(){
		for(let i in datosGraficos){
        	let data = datosGraficos[i]

			let datosGrafico  = {
				labels: data['labels'],
				datasets: [
					{
						data: data['valores'],
						backgroundColor : coloresGrafico
					}
				]
			}

		    //Crea el grafico
			let canvas = $('#grafico' + data['id']).get(0).getContext('2d')
		    let pieChart = new Chart(canvas, {
				type: 'pie',
				data: datosGrafico,
				options: {
			        maintainAspectRatio: false,
			        responsive: true,
				    tooltips: {
						callbacks: {
							label: function(item, data) {
								let dataset = data.datasets[item.datasetIndex]
								let total = dataset.data.reduce(function(valorPrevio, valorActual, indice, arreglo) {
									return valorPrevio + valorActual
								})

								let valorActual = dataset.data[item.index]
								let porcentaje = Math.floor(((valorActual / total) * 100) + 0.5)
								return '  ' + data.labels[item.index] + ': ' + dataset.data[item.index] + ' (' + porcentaje + '%)'
							}
						}
				    }
			    }
		    })
		}
	}

	$(function () {
		setTimeout(function(){
        	cargarCanvas()
		}, 1000)
    })
</script>
</body>
</html>