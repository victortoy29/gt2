<?php require('views/header.php'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Listado de Herramientas</h1>
                    </div>
                    <div class="col-sm-6">
                        <div class="float-sm-right">
                            <div class="btn-group" role="group">
                                <button type="button" id="crearHerramienta" class="btn btn-success">Nueva</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table id="tabla_herramientas" class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre</th>
                                            <th>URL / Enlace</th>
                                            <th>Fase</th>
                                            <th>Estado</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="modalHerramienta" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 id="tituloModal" class="modal-title">Crear Herramienta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formularioHerramienta">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required="required">
                        </div>
                        <div class="form-group">
                            <label for="enlace">URL / Enlace</label>
                            <textarea class="form-control" id="enlace" name="enlace" rows="4" placeholder="URL de la herramienta" required="required"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="fk_fasesproyecto">Fase</label>
                            <select class="form-control" name="fk_fasesproyecto" id="fk_fasesproyecto" required="required">
                                <option value="0">No Aplica</option>
                            </select>
                        </div>
                        <div id="actualizarEstado" class="form-group">
                            <label for="estado">Estado</label>
                            <select class="form-control" name="estado" id="estado" required="required"></select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-submit" id="botonCrearHerramienta" form="formularioHerramienta">Crear</button>
                    <button type="submit" class="btn btn-primary btn-submit" id="botonActualizarHerramienta" form="formularioHerramienta">Actualizar</button>
                </div>
            </div>
        </div>
    </div>

<?php require('views/footer.php');?>
<script type="text/javascript">
    let id
    let tabla = null
    let opciones = []

    function init(info){
        let boton

        if(info.data.length == 0){
            window.location.href = 'main/index'
        } else {
            if(info.data.usuario.rol != 'Administrador'){
                window.location.href = 'main/index'
            }
        }

        enviarPeticion('herramientas', 'obtenerDatosEnum', {columna: 'estado'}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){  
                        $('#estado').append('<option value="'+r.data[i].id+'">'+r.data[i].campo+'</option>')
                    }
                }
            }
        })

        enviarPeticion('fasesproyecto', 'select', {estado: 'Activo'}, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    for (let i = 0; i < r.data.length; i++){  
                        $('#fk_fasesproyecto').append('<option value="'+r.data[i].id+'">'+r.data[i].codigo+': '+r.data[i].nombre+'</option>')
                    }
                }
            }
        })
        
        cargarRegistros({}, 'crear', function(){})

        //Creacion de nueva herramienta
        $('#crearHerramienta').on('click', function(){
            $('#botonCrearHerramienta').show()
            $('#botonActualizarHerramienta').hide()
            $('#actualizarEstado').hide()
            $('#nombre').val('')
            $('#enlace').val('')
            $('#fk_fasesproyecto').val('0')
            $('#estado').val('Activa')
            $('#modalHerramienta').modal('show')
        })
        
        $('.btn-submit').on('click', function(){
            boton = $(this).attr('id')
        })

        $('#formularioHerramienta').on('submit', function(e){
            e.preventDefault()
            let datos = parsearFormulario($(this))
            
            if(boton == 'botonCrearHerramienta'){
	            enviarPeticion('herramientas', 'insert', datos, function(r){
	                if(r.ejecuto == true){
	                    cargarRegistros({id: r.insertId}, 'crear', function(){
	                        $('#modalHerramienta').modal('hide')
				            $('#nombre').val('')
				            $('#enlace').val('')
                            $('#fk_fasesproyecto').val('0')
                            $('#estado').val('Activa')
	                    })
	                } else {
	                    toastr.error('Se produjo un error al crear la herramienta, por favor verifique los datos ingresados e intente de nuevo')
	                }
	            })
            } else {
                datos.id = id

                enviarPeticion('herramientas', 'update', datos, function(r){
                    if(r.ejecuto == true){
                        cargarRegistros({id: id}, 'actualizar', function(){
                            $('#modalHerramienta').modal('hide')
                            $('#nombre').val('')
				            $('#enlace').val('')
                            $('#fk_fasesproyecto').val('0')
                            $('#estado').val('Activa')
                        })
                    } else {
                        toastr.error('Se produjo un error al crear la herramienta, por favor verifique los datos ingresados e intente de nuevo')
                    }
                })
            }
        })
    }
    
    function cargarRegistros(datos, accion, callback){
        //Informacion de las herramientas
        enviarPeticion('herramientas', 'select', datos, function(r){
            if(r.ejecuto == true){
                if(r.data.length > 0){
                    let fila = ''
                    
                    for(let i = 0; i < r.data.length; i++){
                    	let etiqueta = 'success'
                        let enlace = 'N/A'
                        let nombreCodigo = ''

                        if(r.data[i].estado == 'Inactiva'){
                            etiqueta = 'danger'
                        }
                        
                        if(typeof(r.data[i].enlace) != 'undefined'){
                            if(r.data[i].enlace != null){
                                if(r.data[i].enlace != 'N/A'){
                                    enlace = '<a href="'+r.data[i].enlace+'" target="_blank">Link</a>'
                                } else {
                                    enlace = 'N/A'
                                }
                            } else {
                                enlace = 'N/A'
                            }
                        } else {
                            enlace = 'N/A'
                        }

                        if(r.data[i].fk_fasesproyecto != 0){
                            nombreCodigo = r.data[i].fase.codigo + ': ' + r.data[i].fase.nombre
                        } else {
                            nombreCodigo = r.data[i].fase.codigo
                        }

                        fila += '<tr id="herramienta'+r.data[i].id+'">'+
                            '<td align="center">'+r.data[i].id+'</td>'+
                            '<td>'+r.data[i].nombre+'</td>'+
                            '<td>'+enlace+'</td>'+
                            '<td align="center"><div class="badge badge-primary">'+nombreCodigo+'</div></td>'+
                            '<td align="center"><div class="badge badge-'+etiqueta+'">'+r.data[i].estado+'</div></td>'+
                            '<td>'+                                
                                '<button class="btn btn-default btn-sm" onClick="editarHerramienta('+r.data[i].id+')"><i class="fas fa-edit"></i></button>'+
                            '</td>'+
                        '</tr>'
                    }

                    //Datos para reconstruir la tabla
				    let pagina = 0
				    let filtro = ''
				    let orden = [0, 'asc']

                    if(tabla != null){
                        //Si la tabla existe, se destruye para que se pueda crear nuevamente
                        pagina = tabla.page()
                        filtro = tabla.search()
                        orden = [tabla.order()[0][0], tabla.order()[0][1]]
                        tabla.destroy()
                    }

                    //Se agrega o actualiza la fila dependiendo de la accion
                    if(accion == 'crear'){
                        $('#tabla_herramientas tbody').append(fila)
                    } else {
                        $('#herramienta'+r.data[0].id).replaceWith(fila)
                    }

                    tabla = $('#tabla_herramientas').DataTable({
                        'paging': true,
                        'lengthChange': true,
                        'searching': true,
                        'ordering': true,
                        'language': {
                            'lengthMenu': 'Mostrar _MENU_ registros por página',
                            'zeroRecords': 'No hay datos',
                            'info': 'Página _PAGE_ de _PAGES_',
                            'infoEmpty': 'No hay datos disponibles',
                            'infoFiltered': '(filtrado de _MAX_ registros totales)',
                            'search': 'Buscar',
                            'paginate': {'first': 'Primero',
                                        'last': 'Último',
                                        'next': 'Siguiente',
                                        'previous': 'Anterior'
                            }
                        }
                    })

                    if(filtro != ''){
                    	tabla.search(filtro).draw(false)
                    }

                    tabla.page(pagina).order([orden]).draw(false)
                }
            }
            callback()
        })
    }
    
    function editarHerramienta(idHerramienta){
        id = idHerramienta
        
        enviarPeticion('herramientas', 'select', {id: idHerramienta}, function(r){
            if(r.ejecuto == true){
                $.each(r.data[0], function(campo, valor){
                    $('#'+campo).val(valor)
                })
                
                $('#botonCrearHerramienta').hide()
                $('#botonActualizarHerramienta').show()
            	$('#actualizarEstado').show()
                $('#tituloModal').html('Actualizar Herramienta')
                $('#modalHerramienta').modal('show')
            } else {
                toastr.error('Se produjo un error al traer los datos de la herramienta, por favor verifique los datos ingresados e intente de nuevo')
            }
        })
    }
</script>
</body>
</html>